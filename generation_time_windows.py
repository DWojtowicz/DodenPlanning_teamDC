#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
script permettant de gererer des instances de test. 
"""
import numpy as np

"""
methodes qui converti un string de format "hh:mm" en minutes ecoulé depuis 00:01
"""
def conversionMinutes(heureString):
	heureTOKS = heureString.split(":")
	return int(heureTOKS[0])*60 + int(heureTOKS[1])

"""
methodes qui converti des minutes ecoulé depuis 00:01 en string de forme "hh:mm"
"""
def conversionHeures(minutesInteger):
	heures = minutesInteger/60
	minutes = minutesInteger%60
	#on a depassé minuit
	if heures > 23:
		heures = str(23)
	#on est pile a l'heure
	if minutes == 0:
		minutes = str("00")
	res = str(heures)+":"+str(minutes)
	return res


"""
methodes permettant de generer la "diversification" des windows
entree: le pourcentage des TW type a, b ,c et d
a: 5% des tâches ont entre 0 et 15 min de plus
b: 10%  entre 15 et 45min 
c: 45%  entre 45min et 1h
d: 30%  entre 1h et 2h
e: 10% entre 2h et 5h
"""
def diversification(a,b,c,d,e):
	repartition = []
	for i in range(a):
		repartition.append("a")
	for i in range(b):
		repartition.append("b")
	for i in range(c):
		repartition.append("c")
	for i in range(d):
		repartition.append("d")
	for i in range(e):
		repartition.append("e")
	return repartition

"""
methode creant les time wondows.
entre: deux fichier de forme .csv en format "Client,Durée intervention,Date,Début time window,Fin time window" 
		et le nom du fichier de sortie qui aura le meme format
"""
def create_TW(fileName, new_file):
	fichier_entree = open(fileName, 'r')
	fichier_sortie = open("donnees/" + new_file, 'w')
	repartition = diversification(5,10,45,30,10)
	heureDebut = 0
	heureFin = 0
	for ligne in fichier_entree:
		toks = ligne.split(",")
		if toks[0] == "Client":
			fichier_sortie.write(ligne)
		if toks[0] != "Client":
			heureDebut = conversionMinutes(toks[3])
			heureFin = conversionMinutes(toks[4])

			tirage = int( np.random.rand(1)*100)
			
			res = repartition[tirage]
			"""
			a: des tâches ont entre 0 et 15 min de plus
			b: entre 15 et 45min 
			c: entre 45min et 1h
			d: entre 1h et 2h
			e: entre 2h et 5h
			"""
			if res == "a":
				heureDebut -= 15
				heureFin += 15
			elif res == "b":
				heureDebut -= 30
				heureFin += 30
			elif res == "c":
				heureDebut -= 45
				heureFin += 60
			elif res == "d":
				heureDebut -= 60
				heureFin += 120
			elif res == "e":
				heureDebut -= 120
				heureFin += 600

			fichier_sortie.write(toks[0]+ "," + toks[1] + "," + toks[2] + "," + conversionHeures(heureDebut) + "," + conversionHeures(heureFin) + "\n")

	return fichier_sortie


create_TW("donnees/interventions.csv", "test.csv")
#print(conversionMinutes("2:30"))
#print (conversionHeures(conversionMinutes("2:30")))

