#!/usr/bin/env python2
# -*- coding: utf-8 -*-

'''
import googlemaps
from datetime import datetime
now = datetime.now()
gmaps = googlemaps.Client(key='AIzaSyADwYkLgJtZHz1Weig5mQUlWp20tHO-_VY')
directions_result = gmaps.directions("Paris","Toulouse")
#print directions_result[0]["legs"][0]["duration"]["value"]/(60*60.0)
#print directions_result[0]["legs"][0]["distance"]["value"]/1000.0
'''
import os
import sys

import googlemaps
from math import floor
#from datetime import datetime

"""Chemin du fichier d'adresses genere par Java"""
PATH_FICHIER_ADRESSES = "python/adr.tmp"

"""Fichier d'adresses deja genere"""
PATH_HISTORIQUE_ADRESSES = "python/Histadr.txt"

"""Matrice de distances deja generee"""
PATH_HISTORIQUE_MATRICE = "python/Histmatrice.txt"

"""Matrice des distances en sortie, lue par le ZIMPL"""
PATH_MATRICE_DISTANCES_OUT = "zimpl/dist_time.tmp"

"""Clé google"""
KEY = sys.argv[1]

"""Identifiant de API Google"""
gmaps = googlemaps.Client(key=KEY)	

def ecrit(fichier_sortie,origins, destinations, nomsori, nomsdest):
	"""
	Ecris dans le fichier de sortie la matrice entre les origines et les destinations
	"""
	#print origins, "ori"
	#print destinations, "dest"
	#print nomsori, "nomsori"
	#print nomsdest, "nomdest"		
	directions_result = gmaps.distance_matrix( origins, destinations)#, departure_time=now)
	for row in range(len(directions_result["rows"])) :
		for cle in range(len(directions_result["rows"][row]["elements"]) ):
	#		#print noms[row],noms[cle]
		#	direct = directions_result["rows"][row]["elements"][cle]
			fichier_sortie.write(nomsori[row]+"\t" +nomsdest[cle])
			#print nomsori[row],nomsdest[cle]
			stock = ""
			for cle2 in directions_result["rows"][row]["elements"][cle] :
	#			#print cle2
				
				truc = directions_result["rows"][row]["elements"][cle][cle2]
				buff = 0
				
				if (truc != "ZERO_RESULTS" and truc != "NOT_FOUND" ):
					if (cle2 =="duration") :
						buff = truc['value']
					#La duree chez google est exprime en secondes
						stock = str(float(buff)/(60*60))
						#fichier_sortie.write("\t"+ str(buff))
						#print buff
					if (cle2 =="distance"):
						buff = truc['value']
					#La distance chez google est exprime en metre
						fichier_sortie.write("\t"+ str((float(buff))/1000) + "\t" + stock)
						#print buff
				else:
				#print "Erreur : ", truc, row, cle
					#print "infinity", "\n", "infinity"
					#                          DISTANCE km   TEMPS h
					fichier_sortie.write("\t"+"10.0"+ "\t"+ "1.0")
			fichier_sortie.write("\n")
def rassemble(tab):
	"""
	Permet de compiler un tableau en une requete google
	"""
	#if len(toks) >0 and toks[0] != '#':
	first=True
	requete = ""
	for i in range(len(tab)):
		sep = "|"
		if (first):
			first=False
			sep = ""
		
		requete = requete+sep+tab[i]
	return requete

		##print origins, "orig"
def ecrit2(fichier_sortie,origins, destinations, nomsori, nomsdest):
	"""
	Decompose le travail demande a ecrit pour le faire faire 
	petit a petit
	"""
	print origins
	ori = origins
	dest = destinations
	packori = []
	packdest = []
	packnomori = []
	packnomdest = []
	#print "debut"
	#print nomsori
	#print nomsdest
	#print "fin"
	print "Test"
	for i in range(int( floor(len(ori)/5)) ) :
		packori+=[list(ori[i*5:i*5+5])]
		packnomori+=[list(nomsori[i*5:i*5+5])]
		#print len(ori)-len(ori)%5-1, "<-"
	if (len(ori)%5 != 0):
		print len(ori)-len(ori)%5, "<-"
		packori+=[list(ori[len(ori)-len(ori)%5:len(ori)])]
		packnomori+=[list(nomsori[len(nomsori)-len(nomsori)%5:len(nomsori)])]
	
	for i in  range(int( floor(len(dest)/5)) ):
		#temp=[]
		#for j in range(5):
		#temp+=[]
		packdest+=[list(dest[i*5:i*5+5])]
		packnomdest+=[list(nomsdest[i*5:i*5+5])]
	if (len(dest)%5 != 0):
		packdest+=[list(dest[len(dest)-len(dest)%5:len(dest)])]
		packnomdest+=[list(nomsdest[len(nomsdest)-len(nomsdest)%5:len(nomsdest)])]
	for i in range(len (packori)):
		for j in range(len(packdest)):
			ecrit(fichier_sortie, rassemble(packori[i]),rassemble(packdest[j]),packnomori[i], packnomdest[j]  )
	#print packnomori, packori
	#print packnomdest, packdest
	return [packori,packdest]
	

def lecture_gourmande():
	fichier_entree = open(PATH_FICHIER_ADRESSES,'r')
	fichier_sortie = open(PATH_MATRICE_DISTANCES_OUT,'w')
	nomsori = []
	nomsdest = []
	first = True
	first2 = True
	fichier_sortie.write("#No1	No2	 disti1nce 	TimeVoyi1ge\n");
	
	co = ""
	origins = ""
	destinations = ""
	i=0
	k=0
	#Pour chaque ligne du fichier
	for ligne in fichier_entree:
		i+=1
		toks=ligne.split()
		k=0
		#Si la ligne n'est pas vide on rajoute le nom (nom dans adr a origine)
		if len(toks) >0 and toks[0] != '#':
			sep = "|"
			nomsori += [toks[0]]
			if (first):
				first=False
				sep = ""
			
			origins = origins+sep
			for n in range(1,len(toks)):
				origins =origins+toks[n]+" "
		##print origins, "orig"
		#Une fois 6 pers dans origine on fais parein dans destinations
		if (i==5):
			##print "phase 2"
			k=0
			fichier_entree2 = open(PATH_FICHIER_ADRESSES,'r')
			for colonne in fichier_entree2:
				
				k+=1
				##print "+-+" ,i, k				
				toks2=colonne.split()
				if len(toks2) >0 and toks2[0] != '#':
					sep = "|"
					nomsdest += [toks2[0]]
					if (first2):
						first2=False
						sep = ""
					
					destinations = destinations+sep
					for m in range(1,len(toks2)):
						destinations =destinations+toks2[m]+" "
					##print destinations, "dest", k
				if (k==5):
					k=0
					##print "phase 3"
					##print origins, destinations, nomsori, nomsdest
					ecrit(fichier_sortie,origins, destinations, nomsori, nomsdest)
					nomsdest = []
					first2 = True
					destinations = ""
		##print "+++" ,i, k	
		if (k!=0):
			k=0
			ecrit(fichier_sortie,origins, destinations, nomsori, nomsdest)
			nomsdest = []
			first2 = True
			destinations = ""
			fichier_entree2.close
			i=0
			origins=""
			first = True
			nomsori = []


		
	#Cas si i mod 5 != 0 on lance les origines sur les destinations restantes
	##print "+++-" ,i, k
	if (i!=0):
		#ecrit(fichier_sortie,origins, destinations, nomsori, nomsdest)
		k=0
		fichier_entree2 = open(PATH_FICHIER_ADRESSES,'r')
		for colonne in fichier_entree2:
			k+=1
			toks2=colonne.split()
			if len(toks2) >0 and toks2[0] != '#':
				sep = "|"
				nomsdest += [toks2[0]]
				if (first2):
					first2=False
					sep = ""
				
				destinations = destinations+sep
				for i in range(1,len(toks2)):
					destinations =destinations+toks2[i]+" "
				##print destinations, "dest", k
			if (k==5):
				k=0
				##print origins, destinations, nomsori, nomsdest
				ecrit(fichier_sortie,origins, destinations, nomsori, nomsdest)
				nomsdest = []
				first2 = True
				destinations = ""
		fichier_entree2.close	
					
	if (k!=0):
		ecrit(fichier_sortie,origins, destinations, nomsori, nomsdest)
		nomsdest = []
		first2 = True
		destinations = ""
		k=0
		
	origins = co
	destinations = co
#origins=co.split("|")
	#print origins
	#print destinations
	#for i in range(len(noms)):
	#	#print noms[i], toks[i]
	
	#ecrit(fichier_sortie,origins, destinations, nomsori, nomsdest)
			
	fichier_entree.close()
	fichier_sortie.close()
	
	
def lecture_moins_gourmande():
	"""
	Recherche les adresses et leurs distances
	Stoke les adresses et les distances entre elles
	"""
	#fichier_sortie = open("dist_time-v2.txt",'w')
	fichier_sortie = open(PATH_MATRICE_DISTANCES_OUT,'w')
	
	

#	nomsori = []
#	nomsdest = []
#	num=[]
#	adr0 = []
#	for i in fichier_entree:
#		fichier_adr_a.write(i)
#	fichier_adr_a.close	
#	for i in fichier_adr_r:
#		(num,adr, adr0) = i.partition('\t')		
#		#print adr0


	#fichier_entree = open("adr.txt",'r')
	
	fichier_entree = open(PATH_FICHIER_ADRESSES,'r')
	fichier_adr_a = open(PATH_HISTORIQUE_ADRESSES,'a')	
	fichier_adr_r = open(PATH_HISTORIQUE_ADRESSES,'r')
	fichier_matrice_a = open(PATH_HISTORIQUE_MATRICE,'a')
	old_adr = []	
	new_adr =[]
	conserve=[]
	nouvelles=[]
	for i in fichier_adr_r:
		(nom, sep, adresse) = i.partition('\t')
		old_adr +=[[nom,[adresse]]] 
	nouveau_noms = len(old_adr)
	for i in fichier_entree:
		(nom, sep, adresse) = i.partition('\t')
		new_adr+=[[nom,[adresse]]] 
		k=0
		val = True
		
		while (k<len(old_adr) and val) :
			#print adresse
			#print old_adr[k][1][0]
			#print old_adr[k][1][0] == adresse
			if (old_adr[k][1][0] == adresse):
				#print "***++", old_adr[k][1][0] ,nom, adresse
				val = False
			k=k+1
		if (val):
			result = gmaps.geocode(adresse)
		 #print result
			k=0
			if not(len(result)>0):
				temp=adresse.split(",")
				adresse = temp[len(temp)-1]
			while (k<len(old_adr) and val) :
				#print adresse
				#print old_adr[k][1][0]
				#print old_adr[k][1][0] == adresse
				if (old_adr[k][1][0] == adresse):
					#print "***++", old_adr[k][1][0] ,nom, adresse
					val = False
				k=k+1
		if (val):
			k=0
			while (k<len(nouvelles) and val) :
				#print adresse
				#print old_adr[k][1][0]
				#print old_adr[k][1][0] == adresse
				if (nouvelles[k][2] == adresse):
					#print "***++", old_adr[k][1][0] ,nom, adresse
					val = False
				k=k+1
			#print adresse, "truc"
			if (val):
				nouveau_noms = nouveau_noms+1			
				nouvelles+=[[nouveau_noms,nom, adresse]]				
			else:
				conserve+= [[nouvelles[k-1][0],nom, adresse ]]

				
		else:
			#print "conserve"
			conserve += [[k,nom, adresse ]]
	#print conserve
	#print nouvelles
	if (len(nouvelles)!=0 and len(conserve)!=0)	:
		
		nomsori =[]
		destinations=[]
		nomsdest=[]
		origins=[]
		for i in conserve:
			origins += [i[2]]
			#print i[0]
			nomsori += [str(i[0])]
			
		for i in nouvelles:
			#print i[0], str(i[0])
			destinations += [i[2]]
			nomsdest += [str(i[0])]
	
		ecrit2(fichier_matrice_a,origins, destinations, nomsori, nomsdest)
		
		nomsori =[]
		destinations=[]
		nomsdest=[]
		origins=[]
		for i in nouvelles :
			origins += [i[2]]
			#print i[0]
			nomsori += [str(i[0])]
			
		for i in conserve :
			#print i[0], str(i[0])
			destinations += [i[2]]
			nomsdest += [str(i[0])]
		
		ecrit2(fichier_matrice_a,origins, destinations, nomsori, nomsdest)
	
	if (len(nouvelles)!=0)	:
		
		nomsori =[]
		destinations=[]
		nomsdest=[]
		origins=[]
		for i in nouvelles :
			origins += [i[2]]
			#print i[0]
			nomsori += [str(i[0])]
			
		for i in nouvelles :
			#print i[0], str(i[0])
			destinations += [i[2]]
			nomsdest += [str(i[0])]
		
		ecrit2(fichier_matrice_a,origins, destinations, nomsori, nomsdest)
	
		for i in nouvelles:
			buffer0 = str(i[0]) + "\t" + i[2]
			fichier_adr_a.write(buffer0)
			
	fichier_entree.close
	fichier_adr_a.close	
	fichier_adr_r.close
	fichier_matrice_a.close
	
	'''
	Sur l'ensemble des donnees nouvelles ou anciennes
	On cherche les correspondances 
	Si dans l'historique des matrice il y a les valeurs
	on les rajoutes
	'''
	donne = conserve + nouvelles
	if (len(donne)!=0)	:
		
		fichier_matrice_r = open(PATH_HISTORIQUE_MATRICE,'r')
		fichier_sortie.write("#No1	No2	 disti1nce 	TimeVoyi1ge\n")
		#print os.path.getsize(PATH_HISTORIQUE_MATRICE)
		#print donne
		for i in fichier_matrice_r:
			(nomA, sep, adresse) = i.partition('\t')
			(nomB, sep, adresse) = adresse.partition('\t')
			(v1, sep, v2) = adresse.partition('\t')
			#print nomA, nomB, v1,v2	
			kl=0
			kr=0
			valleft = []
			valright = []
			itera = 0
			#print donne[itera][0],int(nomA)
			#print donne, len (donne), "+++"
			while (itera<len(donne)):
				
				#print donne[itera][0], int(nomA), int(nomB)
			# Il y a plusieurs correspondances
				
				if (donne[itera][0]==int(nomA)):
					kl=int(donne[itera][1])
					#print donne[itera]
					valleft+=[int(kl)]
				if (donne[itera][0]==int(nomB)):
					kr=int(donne[itera][1])
					#print donne[itera]
					valright += [int(kr)]
				itera= itera +1
			#if True :#(not(kl==0 or kr==0)):
				#print "impression", valleft, valright
				#print i
			for kl in valleft:
				for kr in valright:
					buffer0=str(kl)+'\t'+str(kr)+'\t'+ str(v1) +'\t'+str(v2)
					#print buffer0
					fichier_sortie.write(buffer0)
				
				

		fichier_matrice_r.close
	fichier_sortie.close
	

def main() :
	"""
	Programme principal du python
	ecrire le code d'appel des fonctions ici
	"""
	lecture_moins_gourmande()
	#lecture_gourmande()
	##print rassemble(["eh","ty","ui"])

def nettoyer() :
	"""
	Fonction nettoyant les fichiers crees qui ne sont plus utiles apres execution du
	programme Python
	"""
	pass

if __name__ == '__main__':
	#main()
	#print "Test_python\n"
	main()
	nettoyer()

#lecture_gourmande()
'''
fichier_matrice_r = open("Histmatrice.txt",'r')
for i in fichier_matrice_r:
	#print "+++----***"
	
'''
# Replace the API key below with a valid API key.


