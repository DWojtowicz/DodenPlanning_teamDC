package gui;

import java.io.IOException;

import gui.model.IntervenantGUI;
import gui.model.UsagerGUI;
import gui.view.BienvenueOverviewController;
import gui.view.GeneralOverviewController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import main.Main;
import model.Intervenant;
import model.Usager;
import utils.CSVReader;
import utils.LinkPythonZIMPL;

/**
 * Classe permettant d'exécuter l'interface graphique
 * @author damien
 *
 */
public class GUIMainApp extends Application {

	/**
	 * Conteneur pour l'interface graphique
	 */
    private Stage primaryStage;
    
    /**
     * Composant racine de l'interface graphique
     */
    private BorderPane rootLayout;
    
	/**
     * Liste des intervenants pour l'IHM
     */
    private ObservableList<IntervenantGUI> donneesIntervenants = FXCollections.observableArrayList();

    /**
     * Liste des usagers pour l'IHM
     */
    private ObservableList<UsagerGUI> donneesUsagers = FXCollections.observableArrayList();
    
    /**
     * Permet de déclarer une nouvelle application.
     * Ne doit pas être exécuté plusieurs fois dans le programme.
     */
    public GUIMainApp() {}
    
    /**
     * Méthode initialisant l'interface graphique
     */
	@Override
	public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("DodenPlanning");
        setUserAgentStylesheet(STYLESHEET_MODENA);
        
        initRootLayout();
        showBienvenueOverview();
    }
	

    /**
     * Méthode pour initialiser la racine de l'interface graphique
     */
    public void initRootLayout() {
        try {
        	
            // Charge la racine RootLayout depuis son FXML
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(GUIMainApp.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Affiche la scene contenant la racine
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
            
        } catch (IOException e) {
        	System.err.println("Impossible de lancer l'interface graphique");
            e.printStackTrace();
        }
    }

    /**
     *Affiche l'interface de bienvenue BienvenueOverview
     */
    public void showBienvenueOverview() {
        try {
            // Charge la vue de bienvenue
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(GUIMainApp.class.getResource("view/BienvenueOverview.fxml"));
            AnchorPane personOverview = (AnchorPane) loader.load();

            // Met la vue au centre de l'interface
            rootLayout.setCenter(personOverview);
            
            // Permet au controlleur d'accéder à son interface
            BienvenueOverviewController controller = loader.getController();
            controller.setMainApp(this);
            controller.customizeDatePicker();
            
        } catch (IOException e) {
            System.err.println("Une erreur s'est produite lors de la mise en place de la vue de bienvenue.");
        	e.printStackTrace();
        }
    }


    /**
     * Affiche l'interface principale dans la racine du programme
     */
    public void showGeneralOverview() {
    	try {
            // Charge la vue générale
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(GUIMainApp.class.getResource("view/GeneralOverview.fxml"));
            AnchorPane personOverview = (AnchorPane) loader.load();

            // Met la vue au centre de l'interface
            rootLayout.setCenter(personOverview);

            // Permet au controlleur d'accéder à son interface
            GeneralOverviewController controller = loader.getController();
            //controller.showInterventions();
            controller.setMainApp(this);
            
        } catch (IOException e) {
            System.err.println("Une erreur s'est produite lors de l'initialisation de l'interface principale.");
        	e.printStackTrace();
        }
    }
    
    /**
     * Permet de passer à la vue générale
     */
    public void changerVue() {
    	//  Génération des fichiers pour le Python et le ZIMPL
    	LinkPythonZIMPL pzdp = LinkPythonZIMPL.getInstance();
    	pzdp.generationFichiers();
    	
    	
    	// Peuplement des classes modèles pour l'IHM
    	for (Intervenant interv : Main.INTERVENANTS)
    		donneesIntervenants.add(new IntervenantGUI(interv));

    	for (Usager usager : Main.USAGERS)
    		donneesUsagers.add(new UsagerGUI(usager));
    	
    	// Affichage de l'interface
    	this.showGeneralOverview();
    }

    
    /**
     * Fonction permettant de charger les données des CSV dans l'appli
     */
    public void loadData() {
    	CSVReader csvReader = CSVReader.getInstance(Main.CHEMIN_INTERVENANTS, Main.CHEMIN_INTERVENTIONS, Main.CHEMIN_USAGERS);
    	csvReader.readUsagerCSV();
    	csvReader.readIntervenantsCSV();
    	csvReader.readInterventionsCSV(Main.DATE_DU_JOUR);
    }

    
    /**
     * Fonction permettant de charger les données des CSV dans l'appli
     */
    public void loadDataBench() {
    	CSVReader csvReader = CSVReader.getInstance(Main.CHEMIN_INTERVENANTS, Main.CHEMIN_INTERVENTIONS, Main.CHEMIN_USAGERS);
    	csvReader.readUsagerBenchCSV();
    	csvReader.readIntervenantsBenchCSV();
    	csvReader.readInterventionsCSV(Main.DATE_DU_JOUR);
    }
    
    /**
     * Interface entre CSVReader et BienvenueOverviewController pour vérifier
     * le fichier d'intervenants
     * @return	True si le fichier d'intervenants est correct, false sinon
     */
    public boolean checkIntervenantCSV() {
    	CSVReader csvReader = CSVReader.getInstance(Main.CHEMIN_INTERVENANTS, Main.CHEMIN_INTERVENTIONS, Main.CHEMIN_USAGERS);
    	return csvReader.checkIntervenantsCSV();
    }

    /**
     * Interface entre CSVReader et BienvenueOverviewController pour vérifier
     * le fichier d'interventions
     * @return	True si le fichier d'interventions est correct, false sinon
     */
    public boolean checkInterventionCSV() {
    	CSVReader csvReader = CSVReader.getInstance(Main.CHEMIN_INTERVENANTS, Main.CHEMIN_INTERVENTIONS, Main.CHEMIN_USAGERS);
    	return csvReader.checkInterventionsCSV();
    }

    /**
     * Interface entre CSVReader et BienvenueOverviewController pour vérifier
     * le fichier d'usagers
     * @return	True si le fichier d'usagers est correct, false sinon
     */
    public boolean checkUsagerCSV() {
    	CSVReader csvReader = CSVReader.getInstance(Main.CHEMIN_INTERVENANTS, Main.CHEMIN_INTERVENTIONS, Main.CHEMIN_USAGERS);
    	return csvReader.checkUsagerCSV();
    }

    /*
     ****************************************************************************
     *		MÉTHODES D'OBJET
     ****************************************************************************
     */
    
    /**
     * Méthode permettant d'exécuter l'interface graphique
     * @param args Les arguments à passer à l'interface. Null en général
     */
	public static void runGUI(String[] args) {
		launch(args);
	}

    /*
     ****************************************************************************
     *		GETTERS ET SETTERS
     ****************************************************************************
     */
    
    /**
     * Returns the data as an observable list of Intervenants. 
     * @return Une liste observable d'intervenants
     */
    public ObservableList<IntervenantGUI> getIntervenantsData() {
        return donneesIntervenants;
    }

    /**
     * Returns the data as an observable list of Usagers. 
     * @return Une liste observable d'usagers
     */
    public ObservableList<UsagerGUI> getUsagersData() {
        return donneesUsagers;
    }

	/**
	 * @return the primaryStage
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}
}
