package gui.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import model.Intervenant;

/**
 * Classe modèle pour l'IHM modélisant un intervenant, son contrat et son adresse
 * @author Damien Wojtowicz
 */
public class IntervenantGUI {

	private final StringProperty id;
	private final StringProperty prenom;
	private final StringProperty nom;
	private final StringProperty voie;
	private final StringProperty commune;
	private final IntegerProperty nbHeuresContrat;
	
	/**
	 * Référence vers l'intervenant inséré dans la liste du Main
	 */
	private Intervenant interv;

	/**
	 * Constructeur initialisant un intervenant de modèle à partir d'un
	 * intervenant des fiches
	 * 
	 * @param intervenant
	 */
	public IntervenantGUI(Intervenant interv) {
		if (interv.getAdresse() != null) {
			this.id = new SimpleStringProperty(interv.getIdIntervenant()+"");
			this.prenom = new SimpleStringProperty(interv.getPrenom());
			this.nom = new SimpleStringProperty(interv.getNom());
			
			this.voie = new SimpleStringProperty(interv.getAdresse().getVoie());
			this.commune = new SimpleStringProperty(interv.getAdresse().getCodePostal() + " " + interv.getAdresse().getCommune());
			
			this.nbHeuresContrat = new SimpleIntegerProperty(interv.getNbHeures());
		} else {
			this.id = new SimpleStringProperty(interv.getIdIntervenant()+"");
			this.prenom = new SimpleStringProperty("");
			this.nom = new SimpleStringProperty("");
			
			this.voie = new SimpleStringProperty("");
			this.commune = new SimpleStringProperty("");
			
			this.nbHeuresContrat = new SimpleIntegerProperty(35);
		}
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom.get();
	}
	
	/**
	 * 
	 * @param prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom.set(prenom);
		interv.setPrenom(prenom);
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom.get();
	}
	
	/**
	 * 
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom.set(nom);
		interv.setNom(nom);
	}

	/**
	 * @return the voie
	 */
	public String getVoie() {
		return voie.get();
	}
	
	/**
	 * 
	 * @param voie
	 */
	public void setVoie(String voie) {
		this.voie.set(voie);
		interv.getAdresse().setVoie(voie);
	}

	/**
	 * @return the commune
	 */
	public String getCommune() {
		return commune.get();
	}
	
	/**
	 * 
	 * @param commune
	 */
	public void setCommune(String commune) {
		this.commune.set(commune);
		interv.getAdresse().setCommune(commune);
	}

	/**
	 * @return the nbHeuresContrat
	 */
	public int getNbHeuresContrat() {
		return nbHeuresContrat.get();
	}
	
	/**
	 * 
	 * @param nbHeures
	 */
	public void setNbHeuresContrat(int nbHeures) {
		this.nbHeuresContrat.set(nbHeures);
	}
	
	/**
	 * 
	 * @return
	 */
	public StringProperty nomProperty() {
		return nom;
	}
	
	/**
	 * 
	 * @param idIntervenant
	 */
	public void setIdIntervenant(int idIntervenant) {
		this.id.set(idIntervenant + "");
		this.interv.setIdIntervenant(idIntervenant);
	}
	
	/**
	 * 
	 * @return
	 */
	public String getIdIntervenant() {
		return id.get();
	}
	
	/**
	 * 
	 * @return
	 */
	public StringProperty idProperty() {
		return id;
	}
	
	/**
	 * 
	 * @return
	 */
	public StringProperty prenomProperty() {
		return prenom;
	}
	
	/**
	 * 
	 * @return
	 */
	public StringProperty voieProperty() {
		return voie;
	}
	
	/**
	 * 
	 * @return
	 */
	public StringProperty communeProperty() {
		return commune;
	}
	
	/**
	 * 
	 * @return
	 */
	public IntegerProperty nbHeuresProperty() {
		return nbHeuresContrat;
	}
	
	/**
	 * 
	 * @return
	 */
	public Intervenant getIntervenant() {
		return interv;
	}
	
}
