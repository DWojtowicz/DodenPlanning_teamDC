package gui.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import model.Usager;

/**
 * Classe modèle pour l'IHM modélisant un intervenant, son contrat et son adresse
 * @author Damien Wojtowicz
 */
public class UsagerGUI {

	private final StringProperty id;
	private final StringProperty prenom;
	private final StringProperty nom;
	private final StringProperty voie;
	private final StringProperty commune;
	
	/**
	 * Référence vers l'usager inséré dans la liste du Main
	 */
	private Usager usager;
	

	/**
	 * Constructeur initialisant un intervenant de modèle à partir d'un
	 * intervenant des fiches
	 * @param usager L'usager à utiliser
	 */
	public UsagerGUI(Usager usager) {
		if (usager.getAdresse() != null) {
			this.id = new SimpleStringProperty(usager.getIdUsager() + "");
			this.prenom = new SimpleStringProperty(usager.getPrenom());
			this.nom = new SimpleStringProperty(usager.getNom());
			
			this.voie = new SimpleStringProperty(usager.getAdresse().getVoie());
			this.commune = new SimpleStringProperty(usager.getAdresse().getCodePostal() + " " + usager.getAdresse().getCommune());
		} else {
			this.id = new SimpleStringProperty(usager.getIdUsager() + "");
			this.prenom = new SimpleStringProperty("");
			this.nom = new SimpleStringProperty("");
			
			this.voie = new SimpleStringProperty("");
			this.commune = new SimpleStringProperty("");
			
		}
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom.get();
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom.get();
	}

	/**
	 * @return the voie
	 */
	public String getVoie() {
		return voie.get();
	}
	
	/**
	 * @return the commune
	 */
	public String getCommune() {
		return commune.get();
	}
	
	/**
	 * 
	 * @return Le nom de l'usager
	 */
	public StringProperty nomProperty() {
		return nom;
	}
	
	/**
	 * 
	 * @return Le prénom de l'usager
	 */
	public StringProperty prenomProperty() {
		return prenom;
	}
	
	/**
	 * 
	 * @return Le nom de la rue
	 */
	public StringProperty voieProperty() {
		return voie;
	}
	/**
	 * 
	 * @return L'usager modélisé
	 */
	public Usager getUsager() {
		return usager;
	}
	
	/**
	 * 
	 * @return Retourne l'identifiant de l'usager
	 */
	public String getIdUsager() {
		return id.get();
	}
	
	/**
	 * 
	 * @return Retourne l'identifiant de l'usager
	 */
	public StringProperty idProperty() {
		return id;
	}
}
