/**
 * Contient les classes modèle pour l'interface graphique. Elles contiennent les 
 * données permettant de remplir les tableaux dans l'IHM.
 */
package gui.model;