/**
 * Contient les classes nécessaires à l'implémentation de l'interface graphique, ainsi 
 * que le point d'entrée de l'interface graphique.
 * <img src="./doc-files/dc_gui.png" alt="DC" />
 */
package gui;