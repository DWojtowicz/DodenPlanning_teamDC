package gui.view;

import java.io.File;
import java.nio.file.Paths;
import java.time.LocalDate;

import gui.GUIMainApp;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import main.Main;

public class BienvenueOverviewController {
	/**
	 * Bouton ouvrant un FileChooser pour les intervenants
	 */
	@FXML
	private Button boutonIntervenants;
	
	/**
	 * Bouton ouvrant un FileChooser pour les interventions
	 */
	@FXML
	private Button boutonInterventions;
	
	/**
	 * Bouton ouvrant un FileChooser pour les usagers
	 */
	@FXML
	private Button boutonUsagers;
	
	/**
	 * Label pour afficher le chemin du fichier d'intervenants
	 */
	@FXML
	private Label labelIntervenantsBienvenue;

	/**
	 * Label pour afficher le chemin du fichier d'interventions
	 */
	@FXML
	private Label labelInterventionsBienvenue;

	/**
	 * Label pour afficher le chemin du fichier d'usagers
	 */
	@FXML
	private Label labelUsagersBienvenue;

	/**
	 * Label pour le message pour la date
	 */
	@FXML
	private Label labelDate;
	
	/**
	 * Bouton pour entrer dans le reste du programme
	 */
	@FXML
	private Button boutinValiderBienvenue;
	
	/**
	 * Bouton pour quitter le programme
	 */
	@FXML
	private Button boutonAnnulerBienvenue;
	
	/**
	 * Permet de sélectionner le jour pour lequel générer le planning
	 */
	@FXML
	private DatePicker datePickerJourIntervention;
	
	/**
	 * Permet de choisir si on veut générer des noms aléatoirement
	 */
	@FXML
	private CheckBox checkBoxBenchmark;
	
	/**
	 * Groupe de Radio Button pour choisir le critère à optimiser
	 */
	@FXML
	private ToggleGroup toggleGroupCritereOpti;
	
	/**
	 * Pour choisir d'optimiser le planning
	 */
	@FXML
	private RadioButton radioOptimiser;
	
	/**
	 * Pour choisir un planning équilibré
	 */
	@FXML
	private RadioButton radioEquilibrer;
	
    /**
     * Référence vers l'application mère
     */
	private GUIMainApp mainApp;

	/**
	 * Permet de choisir les fichiers
	 */
	private final FileChooser fileChooser = new FileChooser();

	/*
	 ***************************************************************************
	 *	EVENT HANDLERS
	 ***************************************************************************
	 */


	/**
	 * Event Listener on Button[#boutinValiderBienvenue].onAction
	 * @param event Le clic sur le bouton
	 */
	@FXML
	public void handleValiderBienvenue(ActionEvent event) {
		boolean passer = true;
		
		/*
		 * Vérification du fichier d'intervenants
		 */
		if (!mainApp.checkIntervenantCSV()) {
			passer = false;
			this.labelIntervenantsBienvenue.setTextFill(Color.RED);
		} else {
			this.labelIntervenantsBienvenue.setTextFill(Color.GREEN);
		}
		
		/*
		 * Vérification du fichier d'interventions
		 */
		if (!mainApp.checkInterventionCSV()) {
			passer = false;
			this.labelInterventionsBienvenue.setTextFill(Color.RED);
		} else {
			this.labelInterventionsBienvenue.setTextFill(Color.GREEN);
		}
		
		/*
		 * Vérification du fichier d'usagers
		 */
		if (!mainApp.checkUsagerCSV()) {
			passer = false;
			this.labelUsagersBienvenue.setTextFill(Color.RED);
		} else {
			this.labelUsagersBienvenue.setTextFill(Color.GREEN);
		}
		
		/*
		 * Vérification de la date saisie
		 */
		if (datePickerJourIntervention.getValue() == null) {
			passer = false;
			this.labelDate.setTextFill(Color.RED);
			
		} else {
			LocalDate ld = this.datePickerJourIntervention.getValue();
			Main.DATE_DU_JOUR = ld.getDayOfMonth() +
					"/" + ld.getMonthValue() +
					"/" + ld.getYear();
			
			this.labelDate.setTextFill(Color.GREEN);
		}
		
		/*
		 * Choix de l'utilisateur sur le ZIMPL à exécuter
		 */
		if (this.radioOptimiser.isSelected())
			Main.PROBLEME_A_RESOUDRE = 0;
		
		else if (this.radioEquilibrer.isSelected())
			Main.PROBLEME_A_RESOUDRE = 1;
		
		// Si l'utilisateur n'a pas renseigné de mauvais fichier,
		// on peut continuer
		if (passer) {
			if (checkBoxBenchmark.isSelected())
				mainApp.loadDataBench();
			else
				mainApp.loadData();
			// Affichage de la vue générale
			mainApp.changerVue();
		}
	}

	/**
	 * Event Listener on DatePicker[#datePickerJourIntervention].onAction
	 * @param event Le clic sur le bouton
	 */
	@FXML
	public void datePickerJourIntervention(ActionEvent event) {
		
	}

	/**
	 * Event Listener on Button[#boutonIntervenants].onAction
	 * @param event Le clic sur le bouton
	 */
	@FXML
	public void handleChoixIntervenantBienvenue(ActionEvent event) {
		configureFileChooser(fileChooser, "intervenants");
		File ficIntervenants = fileChooser.showOpenDialog(mainApp.getPrimaryStage());
		
		// Cas où l'utilisateur n'a pas cliqué sur valider
		if (ficIntervenants != null) {
			Main.CHEMIN_INTERVENANTS = Paths.get(ficIntervenants.getAbsolutePath());
			this.labelIntervenantsBienvenue.setText(ficIntervenants.getAbsolutePath());
		}
	}

	/**
	 * Event Listener on Button[#boutonInterventions].onAction
	 * @param event Le clic sur le bouton
	 */
	@FXML
	public void handleChoixInterventionBienvenue(ActionEvent event) {
		configureFileChooser(fileChooser, "interventions");
		File ficInterventions = fileChooser.showOpenDialog(mainApp.getPrimaryStage());
		
		// Cas où l'utilisateur n'a pas cliqué sur valider
		if (ficInterventions != null) {
			Main.CHEMIN_INTERVENTIONS = Paths.get(ficInterventions.getAbsolutePath());
			this.labelInterventionsBienvenue.setText(ficInterventions.getAbsolutePath());
		}
	}

	/**
	 * Event Listener on Button[#boutonUsagers].onAction
	 * @param event Le clic sur le bouton
	 */
	@FXML
	public void handleChoixUsagerBienvenue(ActionEvent event) {
		configureFileChooser(fileChooser, "usagers");
		File ficUsagers = fileChooser.showOpenDialog(mainApp.getPrimaryStage());

		// Cas où l'utilisateur n'a pas cliqué sur valider
		if (ficUsagers != null) {
			Main.CHEMIN_USAGERS = Paths.get(ficUsagers.getAbsolutePath());
			this.labelUsagersBienvenue.setText(ficUsagers.getAbsolutePath());
		}
	}

	/**
	 * Event Listener on Button[#boutonAnnulerBienvenue].onAction
	 * @param event Le clic sur le bouton
	 */
	@FXML
	public void handleAnnulerBienvenue(ActionEvent event) {
		System.exit(0);
	}


	/*
	 ***************************************************************************
	 *	MÉTHODES DE CLASSE
	 ***************************************************************************
	 */
	
	/**
	 * Fonction permettant de personnaliser l'aspect du file chooser
	 * @param fileChooser
	 * @param type Le message à afficher
	 */
	private void configureFileChooser(final FileChooser fileChooser, String type){                           
		fileChooser.setTitle("Rechercher le fichier pour les " + type);
		fileChooser.setInitialDirectory(
				new File(System.getProperty("user.home"))
				);
		fileChooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter("CSV", "*.csv")
				);
	}
	
	/**
	 * Méthode permettant d'initialiser le DatePicker
	 */
	public void customizeDatePicker() {
		datePickerJourIntervention.setPromptText("dd/mm/yyyy");
	}
	
	
	public void setMainApp(GUIMainApp runBienvenue) {
		this.mainApp = runBienvenue;
	}
}
