package gui.view;

import java.util.ArrayList;

import gui.GUIMainApp;
import gui.model.IntervenantGUI;
import gui.model.UsagerGUI;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import main.Main;
import solution.Solution;
import solution.SolutionAvecAffectations;
import solution.SolutionReader;
import utils.LinkPythonZIMPL;
import utils.LinkSCIP;

public class GeneralOverviewController {
	@FXML
	private TableView<IntervenantGUI> tableIntervenants;
	@FXML
	private TableColumn<IntervenantGUI, String> colIntervenantId;
	@FXML
	private TableColumn<IntervenantGUI, String> colIntervenantNom;
	@FXML
	private TableColumn<IntervenantGUI, String> colIntervenantPrenom;
	@FXML
	private Label voieIntervenantLabel;
	@FXML
	private Label communeIntervenantLabel;
	@FXML
	private Label heuresIntervenantLabel;
	@FXML
	private Label nomIntervenantTitreLabel;
	@FXML
	private Label labelDescrOptimalite;
	@FXML
	private TableView<UsagerGUI> tableUsagers;
	@FXML
	private TableColumn<UsagerGUI, String> colUsagerId;
	@FXML
	private TableColumn<UsagerGUI, String> colUsagerNom;
	@FXML
	private TableColumn<UsagerGUI, String> colUsagerPrenom;
	@FXML
	private Label voieUsagerLabel;
	@FXML
	private Label communeIntervenantLabel1;
	@FXML
	private Label nomUsagerTitreLabel;
	@FXML
	private WebView webViewPlanningParIntervenant;
	@FXML
	private Button afficherButton;
	@FXML
	private Button calculerButton;
	@FXML
	private Label labelOptimalite;
	@FXML
	private TextField textFieldTempsCalcul;
	
    // Reference to the main application.
	@SuppressWarnings("unused")
	private GUIMainApp mainApp;
	
	/**
	 * Web engine pou l'affichage de la liste d'interventions
	 */
	private WebEngine webEngine;
	
	/**
	 * Permet de déterminer si une première solution a déjà été calculée ou non
	 */
	private boolean premierCalcul = true;
	
	/**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    	// Cacher le label Optimalité
    	this.labelDescrOptimalite.setVisible(false);
    	
        // Initialize the intervenants table with the two columns.
    	colIntervenantId.setCellValueFactory(cellData -> cellData.getValue().idProperty());
        colIntervenantNom.setCellValueFactory(cellData -> cellData.getValue().nomProperty());
        colIntervenantPrenom.setCellValueFactory(cellData -> cellData.getValue().prenomProperty());

        // Initialize the usagers table with the two columns.
        colUsagerId.setCellValueFactory(cellData -> cellData.getValue().idProperty());
        colUsagerNom.setCellValueFactory(cellData -> cellData.getValue().nomProperty());
        colUsagerPrenom.setCellValueFactory(cellData -> cellData.getValue().prenomProperty());
        
        // Écoute des changements dans les tables
        tableIntervenants.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showDetailsIntervenant(newValue));
        tableUsagers.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showDetailsUsager(newValue));
        
        // Initialisation du WebEngine pour l'affichage des solutions
        this.webEngine = webViewPlanningParIntervenant.getEngine();
        
        this.textFieldTempsCalcul.setText(LinkSCIP.MAX_EXEC_TIME);
    }
    
    /**
     * Méthode permettant de charger la solution calculée
     */
    private void chargerSolution() {
    	
    	SolutionReader sr = SolutionReader.getInstance();
    	
    	if (Main.SOLUTIONS == null)
    		Main.SOLUTIONS = new ArrayList<Solution>();
    	
    	Main.SOLUTIONS.add(sr.parse());
    }
    
    /**
     * Affiche les interventions dans la WebView
     */
    public void showInterventions() {
    	webEngine.loadContent(Main.SOLUTIONS.get(Main.SOLUTIONS.size() - 1).representationHTML());
    }
    
    /**
     * Affiche à l'écran les détails d'un intervenant
     */
    private void showDetailsIntervenant(IntervenantGUI interv) {
    	if (interv != null) {
    		nomIntervenantTitreLabel.setText(interv.getPrenom() + " " + interv.getNom());
    		voieIntervenantLabel.setText(interv.getVoie());
    		communeIntervenantLabel.setText(interv.getCommune());
    		heuresIntervenantLabel.setText(interv.getNbHeuresContrat() + " heures");
    	} else {
    		nomIntervenantTitreLabel.setText("");
    		voieIntervenantLabel.setText("");
    		communeIntervenantLabel.setText("");
    		heuresIntervenantLabel.setText("");
    	}
    }

    /**
     * Affiche à l'écran les détails d'un usager
     */
    private void showDetailsUsager(UsagerGUI usager) {
    	if (usager != null) {
    		nomUsagerTitreLabel.setText(usager.getPrenom() + " " + usager.getNom());
    		voieUsagerLabel.setText(usager.getVoie());
    		communeIntervenantLabel1.setText(usager.getCommune());
    	} else {
    		nomUsagerTitreLabel.setText("");
    		voieUsagerLabel.setText("");
    		communeIntervenantLabel1.setText("");
    	}
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * @param mainApp L'application principale
     */
    public void setMainApp(GUIMainApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        tableIntervenants.setItems(mainApp.getIntervenantsData());
        tableUsagers.setItems(mainApp.getUsagersData());
    }

	/*
	 ***************************************************************************
	 *	EVENT HANDLERS
	 ***************************************************************************
	 */
    
    /**
     * Appelé quand l'utilisateur appuie sur le bouton Calculer
     */
    @FXML
    private void handleCalculer(ActionEvent event) {
    	/*
    	 * Lance le calcul d'une solution
    	 */

    	// Affiche qu'il est en train de calculer
    	this.webEngine.loadContent("<h1>Calcul en cours...</h1>");
    	
    	// Verrouille le bouton calculer
    	this.calculerButton.setDisable(true);
    	
    	// Récupération du temps de calcul
    	String tempsCalculSaisi = this.textFieldTempsCalcul.getText();
    	LinkSCIP ls = LinkSCIP.getInstance();
    	
    	try {
    		// Lève une exception si la valeur saisie n'est pas un nombre
    		Integer.parseInt(tempsCalculSaisi);
    		
    		// Mise à jour du fichier scip.set si besoin
    		if (!tempsCalculSaisi.equals(LinkSCIP.MAX_EXEC_TIME)) {
    			LinkSCIP.MAX_EXEC_TIME = tempsCalculSaisi;
    			ls.changeParameterFile();
    		}
    	
    	// Cas où l'utilisateur a écrit n'importe quoi
    	} catch (NumberFormatException e) {
    		this.textFieldTempsCalcul.setText(LinkSCIP.MAX_EXEC_TIME);
    	}
    	
    	if (premierCalcul) {
    		// Génèration des fichiers nécessaires au ZIMPL
    		LinkPythonZIMPL lpz = LinkPythonZIMPL.getInstance();
    		lpz.executionPython();
    		premierCalcul = false;
    	}

    	// Calcul d'une solution
    	ls.runSCIP();
    	
    	// Lecture d'une solution
    	this.chargerSolution();
    	    	
    	/*
    	 * Affiche une solution
    	 */
    	
    	this.showInterventions();
    	if (Main.SOLUTIONS.get(Main.SOLUTIONS.size() - 1) instanceof SolutionAvecAffectations) {
    		this.labelDescrOptimalite.setVisible(true);
    		this.labelOptimalite.setText(
    				((SolutionAvecAffectations)Main.SOLUTIONS.get(Main.SOLUTIONS.size() - 1)).getOptimalite() + "%");
    }

    	/*
    	 * Mets à jour le bouton Calculer
    	 */
    	
    	this.calculerButton.setText("Recalculer");
    	this.calculerButton.setDisable(false);
    	
    }
}
