/**
 * Contient les fichiers FXML décrivant l'IHM ainsi que les controlleurs peuplant l'IHM
 * et implémentant les actions de l'utilisateur.
 */
package gui.view;