package main;

import java.nio.file.Path;

/**
 * Classe contenant les méthodes permettant de conduire les benchmarks.
 * Nécessite d'être implémentée.
 * @author damien
 *
 */
public class Benchmark {
	
	/**
	 * Chemin vers la matrice des distances et temps de l'instance de test
	 */
	public static Path MATRICE_DISTANCE_TEMPS;
	
	/**
	 * Durée totale du temps de calcul d'une solution laissé à SCIP
	 */
	public static float TEMPS_TOTAL_SCIP;
	
	/**
	 * Chemin du répertoire contenant les résultats des tests
	 */
	public static Path SORTIE;
	
	/**
	 * Nom du fichier contenant la solution créée par SCIP. Évolue au fil des solutions retournées par SCIP.
	 */
	public static String SORTIE_SOLUTION = "solution_1.csv";
	
	/**
	 * Nom du fichier contenant les statistiques sur la solution. Évolue au fil des solutions retournées par SCIP.
	 */
	public static String SORTIE_SOLUTION_STATS = "solution_stats_1.csv";
	
	/**
	 * Nom du fichier contenant les statistiques globales sur le traitement effectué par SCIP.
	 */
	public static String SORTIE_STATS_FINALES = "statistiques_finales.csv";
	
	/**
	 * Numérotation de la phase de SCIP en cours
	 */
	@SuppressWarnings("unused")
	private int numeroIteration;
	
	/**
	 * Constructeur pour la classe Benchmark
	 */
	public Benchmark() {
		this.numeroIteration = 0;
	}
	
	/**
	 * Méthode implémentant le protocole de test de performances.
	 * Les tests se déroulent de la sorte : 
	 * On exécute le programme ZIMPL une nombre ArrondiEntierSuperieur(Benchmark.TEMPS_TOTAL_SCIP / LinkSCIP.MAX_EXEC_TIME) de fois
	 * À chaque fin d'exécution, on extrait la solution et ses statistiques et on les écrit dans leurs fichiers respectifs
	 * À la fin, on écrit les statistiques finales de l'exécution.
	 */
	public void executerTests() {
		// TODO implémenter le protocole
	}
}
