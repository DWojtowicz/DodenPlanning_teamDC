package main;
import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Path;
import java.util.List;

import gui.GUIMainApp;
import model.Intervenant;
import model.Intervention;
import model.Usager;
import solution.Solution;
import utils.LinkPythonZIMPL;
import utils.LinkSCIP;

/**
 * Point d'entrée du programme
 * @author damien
 *
 */
public class Main {

	/**
	 * La liste des intervenants
	 */
	public static List<Intervenant> INTERVENANTS;

	/**
	 * La liste des interventions
	 */
	public static List<Intervention> INTERVENTIONS;

	/**
	 * La liste des usagers
	 */
	public static List<Usager> USAGERS;

	/**
	 * L'historique des solutions
	 */
	public static List<Solution> SOLUTIONS;


	/**
	 * Le chemin vers le fichier CSV des intervenants
	 */
	public static Path CHEMIN_INTERVENANTS;

	/**
	 * Le chemin vers le fichier CSV des interventions
	 */
	public static Path CHEMIN_INTERVENTIONS;

	/**
	 * Le chemin vers le fichier CSV des usagers
	 */
	public static Path CHEMIN_USAGERS;

	/**
	 * Permet de stocker la date du jour du planning à générer. Au format DD/MM/YYYY
	 */
	public static String DATE_DU_JOUR;
	
	/**
	 * Problème à résoudre. Valeurs possibles :
	 * 		0	Génération du planning sur une journée SANS équilibrage de la charge de travail
	 * 		1	Génération du planning sur une journée AVEC équilibrage de la charge de travail
	 */
	public static int PROBLEME_A_RESOUDRE;

	
	/**
	 * Méthode permettant d'afficher l'aide dans le terminal
	 */
	public static void afficherAide() {
		System.out.println("Utilisation :");
		System.out.println("\tjava -jar DodenPlanning_X.X.X.jar <CHEMIN_VERS_SCIP> <CLÉ_GOOGLE> <ARGUMENTS> [<BENCH>]");
		System.out.println("");
		System.out.println("CHEMIN_VERS_SCIP\n"
				+ "Chemin absolu vers le binaire de SCIP");
		System.out.println("");
		System.out.println("CLÉ_GOOGLE\n"
				+ "Clé Google à utiliser, pour pouvoir accéder à Google Maps");
		System.out.println("");
		System.out.println("ARGUMENTS\n"
				+ "Paramètres de configuration pour SCIP :");
		System.out.println("\t--threads -t");
		System.out.println("\t\tNombre de threads que SCIP peut utiliser.\n"
				+ "\t\tPar défaut : 2");
		System.out.println("\t--count -c");
		System.out.println("\t\tDélai (en secondes) donné à SCIP pour calculer une solution avant de retourner une réponse.\n"
				+ "\t\tPar défaut : 300");
		System.out.println("");
		System.out.println("BENCH\n"
				+ "Mode benchmark, exécute le programme sans interface graphique et écrit les solutions trouvées ainsi que diverses statistiques. Cette option doit être renseignée en dernier.");
		System.out.println("\t--bench");
		System.out.println("\t<FICHIER_INTERVENANTS>\n"
				+ "\t\tLe fichier CSV contenant la liste des intervenants.");
		System.out.println("\t<FICHIERS_INTERVENTIONS>\n"
				+ "\t\tLe fichier CSV contenant la liste des interventions.");
		System.out.println("\t<FICHIER_CLIENTS>\n"
				+ "\t\tLe fichier contenant la liste des clients.");
		System.out.println("\t<MATRICE_DISTANCE_TEMPS>\n"
				+ "\t\tLa matrice des distances et du temps de parcours entre toutes les adresses des données.");
		System.out.println("\t<DATE>\n"
				+ "\t\tLa date pour laquelle l'emploi du temps doit être généré. Au format DD/MM/YYYY");
		System.out.println("\t<TEMPS_TOTAL_SCIP>\n"
				+ "\t\tTemps de calcul total (en secondes) donné à SCIP pour résoudre le problème.");
		System.out.println("\t<INTERVALLE_RETOUR_SOLUTION>\n"
				+ "\t\tDélai (en secondes) donné à SCIP pour calculer une solution avant de retourner une réponse. Écrase la valeur précisée avec l'option --count.");
		System.out.println("\t<SORTIE>\n"
				+ "\t\tLe dossier dans lequel les fichiers de solutions et les statistiques doivent être écrits.");
		System.out.println("\t<ZIMPL>\n"
				+ "\t\tLe problème à résoudre. Valeur numérique comprise dans les valeurs suivantes :\n"
				+ "\t\t\t0\tGénération du planning sur une journée SANS équilibrage de la charge de travail\n"
				+ "\t\t\t1\tGénération du planning sur une journée AVEC équilibrage de la charge de travail");
	}
	
	/**
	 * Fonction permettant de nettoyer les fihiers résiduels de l'exécution
	 */
	public static void nettoyerArborescence() {
		/*
		 *  Racine
		 */
		File repertoire = new File(".");
		viderDossier(repertoire, ".tmp");
		viderDossier(repertoire, ".sol");
		
		/*
		 *  Python
		 */
		repertoire = new File("./python");
		viderDossier(repertoire, ".tmp");
		viderDossier(repertoire, ".sol");
		
		/*
		 *  Zimpl
		 */
		repertoire = new File("./zimpl");
		viderDossier(repertoire, ".tmp");
		viderDossier(repertoire, ".sol");
		
		/*
		 *  Scip
		 */
		repertoire = new File("./scip");
		viderDossier(repertoire, ".tmp");
		viderDossier(repertoire, ".sol");
	}
	
	/**
	 * Méthode pour supprimer tout les fichier d'une extension dans un répertoire.
	 * @param dir Le répertoire à purger
	 * @param extension L'extension de fichier visée. De format ".EXTENSION".
	 */
	private static void viderDossier(File dir, String extension) {
		
		// Récupération des fichiers
		File[] files = dir.listFiles(new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		        return name.toLowerCase().endsWith(extension);
		    }
		});
		
		// Purge
		for (File f : files)
			f.delete();
	}
	
	/**
	 * Point d'entrée du programme
	 * @param args Arguments en entrée de l'application. Voir l'aide.
	 */
	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		// Permet de savoir quel mode du programme on va lancer
		boolean modeBenchmark = false;
		
		/*
		 * Vérification de la présence d'arguments
		 */
		if (args.length < 2) {
			afficherAide();
			System.exit(-1);

		} else {
			// Si le premier argument demande de l'aide, on l'affiche et on quitte
			if (args[0].equals("-h") || args[0].equals("--help")) {
				afficherAide();
				System.exit(1);
			
			// Sinon, on considère que c'est bien le chemin de SCIP et on peut continuer
			} else {
				LinkSCIP.PATH_TO_SCIP = args[0];
				LinkPythonZIMPL.CLE_GOOGLE = args[1];
			}
		}

		/*
		 *  Recherche d'autres arguments
		 */
		for (int i = 2; i < args.length; i++) {
			
			// Cas des threads
			if (args[i].equals("-t") || args[i].equals("--threads")) {
				LinkSCIP.NB_THREADS = args[i+1];
				i++;
				
			// Cas du temps de calcul d'une solution
			} else if (args[i].equals("-c") || args[i].equals("--count")) {
				LinkSCIP.MAX_EXEC_TIME = args[i+1];
				i++;
				
			// Cas du mode benchmark.
			} else if (args[i].equals("--bench") && args.length-i-1 == 9) {
				// Fichier d'intervenants
				// TODO
				i++;
				
				// Fichier d'interventions
				// TODO
				i++;
				
				// Fichier de clients
				// TODO
				i++;
				
				// Matrice des distances et du temps
				// TODO
				i++;
				
				// Date de génération du planning
				// TODO
				i++;
				
				// Temps total pour SCIP
				// TODO
				i++;
				
				// Délai avant le retour d'une solution de SCIP
				// TODO
				i++;
				
				// Dossier dans lequel écrire les résultats
				// TODO
				i++;
				
				// Nature du problème d'optimisation
				// TODO
				
				i++;
				
				// Passage en mode benchmark
				modeBenchmark = true;
				
			// S'il y a encore autre chose, c'est une erreur, on quitte.
			} else {
				afficherAide();
				System.exit(2);
			}
		}
		
		/*
		 * Exécution du programme
		 */
		
		// Mode benchmark : on lance le protocole de tests
		if (modeBenchmark) {
			Benchmark bench = new Benchmark();
			bench.executerTests();
			
		// Cas normal : on lance le programme avec une interface graphique	
		} else {
			GUIMainApp gui = new GUIMainApp();
			gui.runGUI(args);
		}
		
		// Nettoyage de l'espace de fichiers
		nettoyerArborescence();
		
		// Bye bye !
		System.exit(0);
	}
}
