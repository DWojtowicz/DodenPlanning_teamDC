/**
 * Contient le point d'entrée du programme, ainsi que la procédure de tests pour les benchmarks.
 * <img src="./doc-files/dc_main.png" alt="DC" />
 */
package main;