package model;

/**
 * Classe modélisant une adresse
 * @author damien
 *
 */
public class Adresse {
	/**
	 * Identifiant de l'adresse
	 */
	private int idAdresse;
	
	/**
	 * Voie de l'adresse
	 */
	protected String voie;
	
	/**
	 * Commune de l'adresse
	 */
	protected String commune;
	
	/**
	 * Code postal de l'adresse
	 */
	protected String codePostal;
	
	
	/**
	 * Constructeur permettant d'initialiser une adresse.
	 * @param voie La rue
	 * @param codePostal Le code postal
	 * @param commune La commune
	 */
	public Adresse(String voie, String codePostal, String commune) {
		this.voie = voie;
		this.codePostal = codePostal;
		this.commune = commune;
	}

	/**
	 * @return the idAdresse
	 */
	public int getIdAdresse() {
		return idAdresse;
	}
	
	/**
	 * @return the voie
	 */
	public String getVoie() {
		return voie;
	}
	
	/**
	 * @return the commune
	 */
	public String getCommune() {
		return commune;
	}


	/**
	 * @param voie the voie to set
	 */
	public void setVoie(String voie) {
		this.voie = voie;
	}

	/**
	 * @param commune the commune to set
	 */
	public void setCommune(String commune) {
		this.commune = commune;
	}
	
	
	/**
	 * @return the codePostal
	 */
	public String getCodePostal() {
		return codePostal;
	}


	/**
	 * @param codePostal the codePostal to set
	 */
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}


	@Override
	public String toString() {
		return this.voie + ", " + this.commune;
	}
}
