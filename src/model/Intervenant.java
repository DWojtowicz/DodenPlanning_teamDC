package model;

import main.Main;

/**
 * Classe permétant de modéliser un intervenant. Un intervenant est caractérisé par diverses
 * caractéristiques, telles son identifiant, son nom et son prénom ainsi que son adresse.
 * Implémente Comparable afin de pouvoir trier les intervenants par ordre alphabétique dans
 * les affectations
 * @author damien
 *
 */
public class Intervenant implements Comparable<Intervenant> {
	/**
	 * Identifiant de l'Intervenant dans le code
	 */
	private int idIntervenant;
	
	/**
	 * Nom de l'intervenant
	 */
	protected String nom;
	
	/**
	 * Prenom de l'intervenant
	 */
	protected String prenom;
	
	/**
	 * Adresse de l'intervenant
	 */
	protected Adresse adresse;
	
	/**
	 * Nombre d'heures de l'intervenant
	 */
	private int nbHeures;
	

	/*
	 *******************************************************************************************
	 * 		CONSTRUCTEURS
	 ******************************************************************************************* 
	 */
	
	/**
	 * Constructeur null, à n'utiliser qu'à vos risques et périls !!
	 */
	public Intervenant() {}

	/**
	 * Constructeur à utiliser pour créer un intervenant
	 * @param id L'identifiant de l'intervenant
	 * @param prenom Le prénom de l'intervenant
	 * @param nom Le nom de l'intervenant
	 * @param adresse L'adresse de l'intervenant
	 */
	public Intervenant(int id, String prenom, String nom, Adresse adresse) {
		this.idIntervenant = id;
		this.prenom = prenom;
		this.nom = nom;
		this.adresse = adresse;
		this.nbHeures = 8;
	}

	
	/*
	 *******************************************************************************************
	 * 		MÉTHODES DE CLASSE
	 ******************************************************************************************* 
	 */

	@Override
	public String toString() {
		return this.prenom + " " + this.nom;
	}
	
	
	@Override
	public boolean equals(Object o){
		return o instanceof Intervenant
			&& ((Intervenant) o).getIdIntervenant() == this.idIntervenant;
	}
	
	
	@Override
	public int compareTo(Intervenant o) {
		if (equals(o))
			return 0;
		
		if (((Intervenant) o).getIdIntervenant() < this.idIntervenant)
			return -1;
		else
			return 1;
	}
	
	/*
	 *******************************************************************************************
	 * 		MÉTHODES DE NIVEAU OBJET
	 ******************************************************************************************* 
	 */
	
	public static Intervenant getIntervenantParID(int id) {
		/*
		 * Heuristique : On suppose que les intervenants sont ajoutés dans l'ordre de
		 * leurs ID dans la liste globale des intervenants.
		 */
		if (Main.INTERVENANTS.get(id).idIntervenant == id)
			return Main.INTERVENANTS.get(id);
		
		/*
		 * Si l'heuristique n'est pas vérifiée, on parcourt la liste
		 */
		for (Intervenant i : Main.INTERVENANTS)
			if (i.idIntervenant == id)
				return i;
		
		/*
		 * Cas d'erreur : on ne peut pas récupérer d'intervenant qui n'existe pas.
		 */
		throw new IllegalArgumentException("L'intervenant " + id + " n'existe pas.");
	}
	
	/*
	 *******************************************************************************************
	 * 		GETTERS ET SETTERS
	 ******************************************************************************************* 
	 */

	public int getIdIntervenant() {
		return idIntervenant;
	}

	public void setIdIntervenant(int idIntervenant) {
		this.idIntervenant = idIntervenant;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public int getNbHeures() {
		return nbHeures;
	}

	public void setNbHeures(int nbHeures) {
		this.nbHeures = nbHeures;
	}
	
}
