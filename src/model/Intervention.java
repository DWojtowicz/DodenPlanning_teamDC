package model;

import java.util.Date;
import main.Main;

/**
 * Permet de modéliser une intervention, c'est à dire une intervention nécessaire chez un client.
 * @author damien
 *
 */
public class Intervention {
	
	/**
	 * Identifiant d'un intervenant
	 */
	private int idIntervention;

	/**
	 * Infos intervention
	 */
	protected String nom;
	
	/**
	 * Usager de l'intervention
	 */
	protected Usager beneficiaire;
	
	/**
	 * Durée en minute de l'intervention
	 */
	protected float duree;
		

	/**
	 * Début de la fenêtre temporelle de l'intervention
	 */
	protected Date debutFenetreTemporelle;
	
	/**
	 * Fin de la fenêtre temporelle de l'intervention 
	 */
	protected Date finFenetreTemporelle;

	
	/**
	 * Code Identifiant de l'intervention
	 */
	protected String codeIdentifiant;
	
	/**
	 * Constructeur permettant d'initialiser une intervention à partir du format dans
	 * les fichiers CSV en entrée
	 * @param id L'identifiant de l'intervention
	 * @param beneficiaire L'usager pour l'intervention
	 * @param duree La durée de l'intervention
	 * @param debutTW Le début du créneau horaire possible de l'intervention
	 * @param finTW La fin du créneau horaire possible de l'intervention
	 */
	public Intervention(int id, Usager beneficiaire, int duree, Date debutTW, Date finTW) {
		super();
		this.idIntervention = id;
		this.beneficiaire = beneficiaire;
		this.duree = duree;
		this.debutFenetreTemporelle = debutTW;
		this.finFenetreTemporelle = finTW;
	}

	public int getIdIntervention() {
		return idIntervention;
	}

	public void setIdIntervention(int idIntervention) {
		this.idIntervention = idIntervention;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Usager getBeneficiaire() {
		return beneficiaire;
	}

	public void setBeneficiaire(Usager beneficiaire) {
		this.beneficiaire = beneficiaire;
	}

	public float getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}


	public Date getDebutFenetreTemporelle() {
		return debutFenetreTemporelle;
	}

	public void setDebutFenetreTemporelle(Date jourDebut) {
		this.debutFenetreTemporelle = jourDebut;
	}

	public Date getFinFenetreTemporelle() {
		return finFenetreTemporelle;
	}

	public void setFinFenetreTemporelle(Date jourFin) {
		this.finFenetreTemporelle = jourFin;
	}

	public String getCodeIdentifiant() {
		return codeIdentifiant;
	}

	public void setCodeIdentifiant(String codeIdentifiant) {
		this.codeIdentifiant = codeIdentifiant;
	}

	/**
	 * Fonction permettant de retourner une référence vers une intervention dans
	 * la liste Main.interventions
	 * @param id Le code de l'intervention
	 * @return une référence vers l'intervention de code id
	 */
	public static Intervention getInterventionParID(int id) {
		/*
		 * Heuristique : On suppose que les interventions sont ajoutées dans l'ordre de
		 * leurs ID dans la liste globale des interventions.
		 */
		System.out.println("BUUUG : " + id);
		if (Main.INTERVENTIONS.get(id).idIntervention == id)
			return Main.INTERVENTIONS.get(id);
		
		/*
		 * Si l'heuristique n'est pas vérifiée, on parcourt la liste
		 */
		for (Intervention i : Main.INTERVENTIONS)
			if (i.idIntervention == id)
				return i;
		
		/*
		 * Cas d'erreur : on ne peut pas récupérer d'intervenant qui n'existe pas.
		 */
		throw new IllegalArgumentException("L'intervention " + id + " n'existe pas.");
	}
	
	/**
	 * méthode permettant de determiner si 2 interventions sont egaux
	 * @param o L'intervention à comparer
	 * @return boolean egaux
	 */
	@Override
	public boolean equals(Object o){
		return o instanceof Intervention
			&& ((Intervention) o).getIdIntervention() == this.idIntervention;

	}
}
