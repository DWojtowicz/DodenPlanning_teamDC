package model;

import java.util.List;

import main.Main;

/**
 * Classe modélisant un usager.
 * @author damien
 *
 */
public class Usager {
	
	/**
	 * Identifiant de l'usage
	 */
	private int idUsager;
	
	/**
	 * Nom de l'usager
	 */
	protected String nom;
	
	/**
	 * Prenom de l'usager
	 */
	protected String prenom;
	
	/**
	 * Liste des préférances
	 */
	protected List<Integer> preferences;
	
	/**
	 * Adresse de l'intervenant
	 */
	protected Adresse adresse;
	
	/**
	 * 
	 */
	protected List<Intervention> interventionsSubies;

	public Usager() {}
	
	/**
	 * @param prenom
	 * @param nom
	 * @param adresse
	 */
	public Usager(String prenom, String nom, Adresse adresse) {
		this.prenom = prenom;
		this.nom = nom;
		this.adresse = adresse;
	}


	public Usager(String nom, Adresse adresse) {
		super();
		this.nom = nom;
		this.adresse = adresse;
	}
	
	/***
	 * 
	 * @param id
	 * @param prenom
	 * @param nom
	 * @param adresse
	 */
	public Usager(int id, String prenom, String nom, Adresse adresse) {
		this.idUsager = id;
		this.prenom = prenom;
		this.nom = nom;
		this.adresse = adresse;
	}

	public int getIdUsager() {
		return idUsager;
	}

	public void setIdUsager(int idUsager) {
		this.idUsager = idUsager;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public List<Integer> getPreferences() {
		return preferences;
	}

	public void setPreferences(List<Integer> preferences) {
		this.preferences = preferences;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}
	
	
	/**
	 * Fonction permettant de réccupérer une instance d'unsager dans la liste Main.usagers
	 * @param id
	 * @return
	 */
	public static Usager getUsagerParID(int id) {
		/*
		 * Heuristique : On suppose que les usagers sont ajoutés dans l'ordre de
		 * leurs ID dans la liste globale des usagers.
		 */
		if (Main.USAGERS.get(id).idUsager == id)
			return Main.USAGERS.get(id);
		
		/*
		 * Si l'heuristique n'est pas vérifiée, on parcourt la liste
		 */
		for (Usager u : Main.USAGERS)
			if (u.idUsager == id)
				return u;
		
		/*
		 * Cas d'erreur : on ne peut pas récupérer d'usager qui n'existe pas.
		 */
		throw new IllegalArgumentException("L'usager " + id + " n'existe pas.");
	}
	
	
	@Override
	public String toString() {
		return this.prenom + " " + this.nom;
	}
}