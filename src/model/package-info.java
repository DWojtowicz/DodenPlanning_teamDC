/**
 * Package contenant les classes stockant les données "métier".
 * Le nom "fiches" est historique : il provient de la description du format des
 * données d'intervenants et d'interventions qui a été transmise par l'UNA.
 * Aujourd'hui, ces classes n'ont plus rien à voir avec ces fiches.
 * <img src="./doc-files/dc_model.png" alt="DC" />
 */
package model;