package solution;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import model.Intervenant;
import model.Intervention;

/**
 * Classe permettant d'afecter à une heure précise une intervention
 * @author damien
 */
public class AffectationIntervention {

	/**
	 * L'intervention affectée
	 */
	private Intervention intervention;

	/**
	 * L'intervenant affecté à l'intervention
	 */
	private Intervenant intervenant;

	/**
	 *L'heure à laquelle l'intervention affectée commence
	 */
	private Date heureDebut;

	/**
	 * L'heure à laquelle l'intervention affectée se termine
	 */
	private Date heureFin;

	/**
	 * Constructeur permettant d'instancier une AffectationIntervention
	 * @param i L'intervention à affecter
	 * @param heureDebut L'heure de début d'une intervention
	 * @param heureFin L'heure de fin d'une intervention
	 * @param interv L'intervenant affecté à l'intervention 
	 */
	public AffectationIntervention(Intervention i, Date heureDebut, Date heureFin, Intervenant interv) {
		this.intervention = i;
		this.heureDebut = heureDebut;
		this.heureFin = heureFin;
		this.intervenant = interv;
	}

	public Intervention getIntervention() {
		return intervention;
	}

	public Date getHeureDebutIntervention() {
		return this.heureDebut;
	}


	public Date getHeureFinIntervention() {
		return this.heureFin;
	}

	public Intervenant getIntervenant() {
		return intervenant;
	}

	public void setHeureDebut(Date d) {
		this.heureDebut = d;
	}

	public void setHeureFin(Date d) {
		this.heureFin = d;
	}

	@Override
	public String toString() {
		String ret = "";
		String pattern = "H:mm";
		SimpleDateFormat df = new SimpleDateFormat(pattern);

		//Convert Date to String
		ret += df.format(heureDebut);
		ret += " à ";
		ret += df.format(heureFin);
		ret += " chez " ;
		ret += intervention.getBeneficiaire().toString();

		return ret;
	}

	/**
	 * Fonction permettant de récupérer une référence d'affectectation dans 
	 * une Map d'affecatations par intervenant
	 * à partir d'un identifiant d'intervention
	 * @param roster Les affectations
	 * @param idIntervention Le numér de l'intervention à trouver
	 * @return Une réf vers une AffectationIntervention
	 */
	public static AffectationIntervention getAffectation(Map<Intervenant, List<AffectationIntervention>> roster, int idIntervention) {

		/*
		 * Parcours de la liste des affectations
		 */
		for (Intervenant i : roster.keySet())
			for (AffectationIntervention a : roster.get(i))
				if (a.intervention.getIdIntervention() == idIntervention)
					return a;

		/*
		 * Cas d'erreur : on ne peut chercher une affectation non affectée
		 */
		throw new IllegalArgumentException("L'intervention " + idIntervention + " n'existe pas ou n'est pas (encore) affectée.");
	}
}
