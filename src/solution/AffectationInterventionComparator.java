package solution;

import java.util.Comparator;

/**
 * Classe permettant de comparer deux affectations d'interventions.
 * @author damien
 *
 */
public class AffectationInterventionComparator implements Comparator<AffectationIntervention> {

	@Override
	public int compare(AffectationIntervention o1, AffectationIntervention o2) {
		return o1.getHeureDebutIntervention().compareTo(o2.getHeureDebutIntervention());
	}

}
