package solution;

/**
 * Interface permettant de manipuler des solutions
 * @author damien
 *
 */
public abstract class Solution {
	
	/**
	 * Méthode permettant de représenter une solution au format HTML
	 * @return Le code HTML représentant la solution
	 */
	public abstract String representationHTML();

}
