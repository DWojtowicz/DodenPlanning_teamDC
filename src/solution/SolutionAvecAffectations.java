package solution;

import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import model.Intervenant;

/**
 * 
 * @author damien
 *
 */
public class SolutionAvecAffectations extends Solution {
	
	/**
	 * L'ensemble des affectations 
	 */
	protected TreeMap<Intervenant, List<AffectationIntervention>> affectations;
	
	/**
	 * Le temps de travail par intervenant
	 */
	protected TreeMap<Intervenant, Float> tempsTravailParIntervenant;

	/**
	 * Le temps d'attente par intervenant
	 */
	protected TreeMap<Intervenant, Float> tempsAttenteParIntervenant;

	/**
	 * Le temps de voyage par intervenant
	 */
	protected TreeMap<Intervenant, Float> tempsVoyageParIntervenant;

	/**
	 * Heure de départ par intervenant
	 */
	protected TreeMap<Intervenant, Date> heureDepartParIntervenant;

	/**
	 * Heure de retour par intervenant
	 */
	protected TreeMap<Intervenant, Date> heureRetourParIntervenant;
	
	/**
	 * Pourcentage d'optimalité de la solution. 0.0% = optimal, 100.0% = raté 
	 */
	protected float optimalite = 100.0f;
	
	/**
	 * Total du temps de voyage et d'attente de la solution
	 */
	protected float tempsNonTravailleTotal;

	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * @return the solution
	 */
	public TreeMap<Intervenant, List<AffectationIntervention>> getAffectations() {
		return affectations;
	}

	/**
	 * @return the tempsTravailParIntervenant
	 */
	public TreeMap<Intervenant, Float> getTempsTravailParIntervenant() {
		return tempsTravailParIntervenant;
	}

	/**
	 * @return the tempsAttenteParIntervenant
	 */
	public TreeMap<Intervenant, Float> getTempsAttenteParIntervenant() {
		return tempsAttenteParIntervenant;
	}

	/**
	 * @return the tempsVoyageParIntervenant
	 */
	public TreeMap<Intervenant, Float> getTempsVoyageParIntervenant() {
		return tempsVoyageParIntervenant;
	}

	/**
	 * @return the optimalite
	 */
	public float getOptimalite() {
		return optimalite;
	}

	/**
	 * @return the tempsNonTravailleTotal
	 */
	public float getTempsNonTravailleTotal() {
		return tempsNonTravailleTotal;
	}

	@Override
	public String representationHTML() {
		String html = "";

//	Format du HTML :
//    	<h1>Intervenant 1</h1>
//    	<h2>Stats</h2>
//		<ul>
//			<li>Stat1</li>
//			<li>Stat1</li>
//			<li>Stat1</li>
//		</ul>
//    	<h2>Interventions</h2>
//    	<ul>
//    		<li>Intervention</li>
//    		<li>Intervention</li>
//    	</ul>
		for (Intervenant i : this.affectations.keySet()) {
			html += "<h1>";
			
			// Nom de l'intervenant
			html += i.toString() + "</h1>";
			
			html += "<h2>Statistiques</h2><ul>";
			html += "<li><b>Temps de travail :</b> " + this.tempsTravailParIntervenant.get(i) + " heures</li>";
//	    	html += "<li><b>Temps d'attente :</b> " + this.tempsAttenteParIntervenant.get(i) + " heures</li>";
//	    	html += "<li><b>Temps de voyage :</b> " + this.tempsVoyageParIntervenant.get(i) + " heures</li>";
	    	html += "<li><b>Temps hors intervention :</b> " + this.tempsVoyageParIntervenant.get(i) + " heures</li>";
			
	    	html += "</ul><h2>Interventions</h2><ul>";
	    	
			// Liste des affectations
			for (AffectationIntervention af : this.affectations.get(i))
    			html += "<li>" + af.toString() + "</li>";

    		html += "</ul>";
		}
		
		
		return html;
	}
	

	
}
