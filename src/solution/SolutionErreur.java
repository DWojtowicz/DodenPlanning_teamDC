package solution;

/**
 * Modélisation d'une erreur dans les calculs
 * @author damien
 *
 */
public class SolutionErreur extends Solution {

	/**
	 * Message d'erreur à afficher en cas de problème
	 */
	private String messageErreur;
	
	/**
	 * Constructeur pour une solution avec erreur
	 * @param messageErreur Le message d'erreur à afficher
	 */
	public SolutionErreur(String messageErreur) {
		this.messageErreur = messageErreur;
	}
	
	@Override
	public String representationHTML() {
		return "<h1>Une erreur s'est produite&nbsp;!</h1>" +
				"<p style=\"color : red;\">" + this.messageErreur + "</p>";
	}

}
