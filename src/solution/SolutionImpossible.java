package solution;

/**
 * Modélise une solution non calculable
 * @author damien
 *
 */
public class SolutionImpossible extends Solution {

	@Override
	public String representationHTML() {
		return "<p>Il est impossible de calculer un planning à partir de ces données.</p>";
	}

}
