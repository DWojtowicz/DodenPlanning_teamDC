package solution;

/**
 * Méthode permettant de modéliser une solution non encore calculée
 * @author damien
 *
 */
public class SolutionPasEncoreCalculee extends Solution {

	@Override
	public String representationHTML() {
		return "<p>Il n'y a pas assez de temps pour calculer le plannig.</p>";
	}

}
