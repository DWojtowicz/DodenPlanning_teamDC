package solution;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import main.Main;
import model.Intervenant;
import model.Intervention;
import utils.LinkSCIP;

/***
 * 
 * Singleton permettant de fabriquer une solution à partir de la sortie de SCIP.
 * 
 * @author charlotte
 *
 */
public class SolutionReader {


	/*
	 * Le temps de voyage retourné par SCIP comprend aussi le trajet maison-travail
	 * Il faut modifier le temps de voyage de la solution pour prendre en compte ça
	 * (enlever trajet maison-travail)
	 * 
	 * Pour l'instant, tempsVoyageParIntervenant = tempsAttenteParIntervenant
	 */

	/**
	 * Le singleton
	 */
	private static volatile SolutionReader INSTANCE = null;

	/**
	 * Constructeur privé, il faut utiliser getInstance pour récupérer
	 * une classe ReadSolution
	 */
	private SolutionReader() {}

	/**
	 * Fonction permettant de récupérer l'instance de ReadSolution.
	 * @return L'instannce de ReadSolution
	 * @see https://en.wikipedia.org/wiki/Singleton_pattern
	 */
	public static SolutionReader getInstance() {
		if (INSTANCE == null) {
			synchronized(SolutionReader.class) {
				if (INSTANCE == null) {
					INSTANCE = new SolutionReader();
				}
			}
		}
		return INSTANCE;
	}

	/**
	 * Fonction lisant dans le fichier de résultat d'exécution et retournant
	 * une Solution
	 * @return une Solution
	 */
	public Solution parse() {
		List<String> lignes = null;
		Path file = Paths.get(LinkSCIP.NOM_FICHIER_RES_EXECUTION);

		try {
			lignes = Files.readAllLines(file);
			
			// Cas où il y a eu un souci
			if (lignes == null || lignes.size() == 0 || lignes.get(0).contains("unknown")) {
				System.out.println("unknown");
				return new SolutionErreur("Erreur du solveur.");
				
			// Cas où il y a eu un manque de temps
			} else if (lignes.get(0).contains("time limit reached") && lignes.size() == 2) {
				System.out.println("time limit reached");
				return new SolutionPasEncoreCalculee();
				
			// Cas où une solution n'est pas calculable
			} else if (lignes.get(0).contains("infeasible")){
				System.out.println("infeasible");
				return new SolutionImpossible();
			
			// Cas où une solution optimale est trouvée
			} else  if (lignes.get(0).contains("optimal solution found")) {
				System.out.println("optimal solution found");
	 			return parseSolutionAvecAffectation();	
			
	 		// Cas où une solution non optimale est trouvée
			} else if (lignes.get(0).contains("time limit reached") && lignes.size() > 2) {
				System.out.println("time limit reached - solution");
				return parseSolutionAvecAffectation();
			}
			
			// Cas où on n'a pas prévu de sortie
			return new SolutionErreur("Erreur imprévue.");
 			
 			
		} catch (IOException e) {
			// S'il y a eu une erreur
			return new SolutionErreur("Erreur de lecture de fichier.");
		}
	}
	
	/**
	 * Méthode permettant de lire une solution faisable.
	 * On suppose que le fichier contenant la solution est correct.
	 * @return Une solution avec des intervations affectées à des intervenants
	 */
	private SolutionAvecAffectations parseSolutionAvecAffectation() {
		/*
		 * Déclaration des variables de Solution
		 */
		SolutionAvecAffectations laSoluce = new SolutionAvecAffectations();
		TreeMap<Intervenant, List<AffectationIntervention>> affectations = new TreeMap<>();
		TreeMap<Intervenant, Float> tempsTravailParIntervenant = new TreeMap<>();
		TreeMap<Intervenant, Float> tempsAttenteParIntervenant = new TreeMap<>();
		TreeMap<Intervenant, Date> heureDepartParIntervenant = new TreeMap<>();
		TreeMap<Intervenant, Date> heureRetourParIntervenant = new TreeMap<>();
		float optimalite = 100.0f;		// Valeur par défaut
		float tempsNonTravailleTotal = 0.0f;

		/*
		 * Lecture du fichier
		 */
		List<String> lignes = null;
		Path file = Paths.get(LinkSCIP.NOM_FICHIER_RES_EXECUTION);

		try {
			lignes = Files.readAllLines(file);

			/*
			 * Parcours des lignes
			 */
			String[] toks;	// Décomposition d'une ligne
			String[] subToks;	// Décomposition d'une partie d'une ligne
			int noIntervention;
			int noIntervenant;
			float nbHeures;
			Intervenant intervenant;
			Intervention intervention;
			Date date;
			AffectationIntervention aff;
			List<AffectationIntervention> affects;

			for (String ligne : lignes) {
				// Découpe la ligne en supprimant les espaces
				toks = ligne.split("\\s+");

				// TODO éclater le code des case dans des sous-fonctions
				
				switch (toks[0]) {
				// Optimalité
				// Lorsqu'une solution non optimale est trouvée, il y a une ligne contenant "Gap"
				//Lorsqu'une solution optimale est trouvée, il y a écrit "solution status: optimal solution found"
				case "Gap" :
					optimalite = Float.parseFloat(toks[2]);
					break;

				// Temps perdu, en heures
				case "objective" :
					tempsNonTravailleTotal = Float.parseFloat(toks[2]);
					break;

				// cas pour lire les intervenants et les interventions
				default :
					subToks = toks[0].split("#");
					switch(subToks[0]) {
					// Donne le numéro d'intervention associée au numéro d'intervenant
					case "idInte":
						noIntervention = Integer.parseInt(subToks[1]) - Main.INTERVENANTS.size() - 1;
						noIntervenant = Integer.parseInt(toks[1]) - 1;
						intervenant = Intervenant.getIntervenantParID(noIntervenant);
						intervention = Intervention.getInterventionParID(noIntervention);

						// Plusieurs cas : l'intervenant a déjà eu d'affectations
						if (affectations.containsKey(intervenant)) {
							affectations.get(intervenant)
							.add(new AffectationIntervention(intervention, null, null, intervenant));
						}
						// Ou bien, il n'en a pas eu
						else {
							affects = new ArrayList<AffectationIntervention>();
							affects.add(new AffectationIntervention(intervention, null, null, intervenant));
							affectations.put(intervenant, affects);
						}
						break;

						// Associe à chaque intervention le moment où elle commence
					case "TArriReal":
						noIntervention = Integer.parseInt(subToks[1]) - Main.INTERVENANTS.size() - 1;								
						aff = AffectationIntervention.getAffectation(affectations, noIntervention);
						aff.setHeureDebut(parseDate(toks[1]));
						break;

						// Associe à chaque intervention le moment où elle se termine
					case "TLeavReal" :
						noIntervention = Integer.parseInt(subToks[1]) - Main.INTERVENANTS.size() - 1;
						aff = AffectationIntervention.getAffectation(affectations, noIntervention);
						aff.setHeureFin(parseDate(toks[1]));
						break;

						// Associe à chaque intervenant le moment où il part de chez lui
					case "TDepReal" :
						intervenant = Intervenant.getIntervenantParID(Integer.parseInt(subToks[1]) - 1);
						date = parseDate(toks[1]);
						heureDepartParIntervenant.put(intervenant, date);
						break;

						// Associe à chaque intervenant le moment où il rentre chez lui
					case "TRetReal" :
						intervenant = Intervenant.getIntervenantParID(Integer.parseInt(subToks[1])- 1);
						date = parseDate(toks[1]);
						heureRetourParIntervenant.put(intervenant, date);
						break;

						// Associe à chaque intervenant son temps de travail effectif (seulement les interventions) 
					case "TWorReal" :
						intervenant = Intervenant.getIntervenantParID(Integer.parseInt(subToks[1])- 1);
						nbHeures = Float.parseFloat(toks[1]);
						tempsTravailParIntervenant.put(intervenant, nbHeures);
						break;
					}

				}

			}

			/*
			 * Construction de la Solution
			 */
			
	    	// Tri des affectations
			for (Intervenant i : affectations.keySet())
				Collections.sort(affectations.get(i), new AffectationInterventionComparator());

			tempsAttenteParIntervenant = tempsNonTravaille(affectations, tempsTravailParIntervenant);

			laSoluce.affectations = affectations;
			laSoluce.tempsAttenteParIntervenant = tempsAttenteParIntervenant;
			laSoluce.tempsTravailParIntervenant = tempsTravailParIntervenant;
			/*
			 *  TODO Faire la différence entre déplacement et attente. COMPLQIUÉ : il faudrait avoir la matrice des distances dans le programme Java.
			 */
			laSoluce.tempsVoyageParIntervenant = laSoluce.tempsAttenteParIntervenant;
			laSoluce.heureDepartParIntervenant = heureDepartParIntervenant;
			laSoluce.heureRetourParIntervenant = heureRetourParIntervenant;
			laSoluce.optimalite = optimalite;
			laSoluce.tempsNonTravailleTotal = tempsNonTravailleTotal;
			System.out.println(affectations.size());
		} catch (IOException e) { // Cas où la lecture des lignes est impossible
			e.printStackTrace();
		}

		/*
		for (List<AffectationIntervention> a:laSoluce.affectations.values()) {
			for (AffectationIntervention b:a) {
				System.out.println(b.getIntervenant().getNom() + " " + (b.getIntervention().getIdIntervention()+Main.intervenants.size() +1 ));
			}

		}*/

		return laSoluce;
	}

	/**
	 * Fonction parsant une heure sortie dans SCIP
	 * @param tok Une heure au format N.N
	 * @return Un objet Date au jour du premier avril 2017, à l'heure et à la minute venant du token
	 */
	@SuppressWarnings("deprecation")
	private Date parseDate(String heure) {
		int minutes = 0;
		String[] toks = heure.split("\\.");
		int heures = (int)Integer.parseInt(toks[0]);
		// recuperer les minutes
		if (toks.length > 1) {
			minutes = (int)((Float.parseFloat(heure) - heures) * 60);
		}

		// TODO Améliorer ça quand on fera le support des dates
		// TODO Contourner la dépréciation
		// On considère que l'on ne travaille qu'avec une date donnée.
		return new Date(2017, 4, 1, heures, minutes);
	}

	/**
	 * Fonction calculant le temps de déplacement et d'attente d'un intervenant
	 * en ne tenant pas en compte le temps de trajet domicile-travail, ce que
	 * ne fait pas SCIP.
	 * @param affectations
	 * @param tempsTravailParIntervenant
	 * @return
	 */
	private TreeMap<Intervenant, Float> tempsNonTravaille(
			TreeMap<Intervenant, List<AffectationIntervention>> affectations,
			TreeMap<Intervenant, Float> tempsTravailParIntervenant) {

		TreeMap<Intervenant, Float> tempsAttenteParIntervenant = new TreeMap<>();
		float temps;

		for (Intervenant i : affectations.keySet()) {
			// Récupération de la durée de la journée, en minutes
			temps = (float)TimeUnit.MILLISECONDS.toMinutes(
					affectations.get(i).get(affectations.get(i).size()-1).getHeureFinIntervention().getTime() -
					affectations.get(i).get(0).getHeureDebutIntervention().getTime());

			// Conversion en heures
			temps /= 60;

			// Soustraction du temps de travail en intervention
			temps -= tempsTravailParIntervenant.get(i);

			// Ajout du couple
			tempsAttenteParIntervenant.put(i, temps);
		}

		return tempsAttenteParIntervenant;
	}



}

