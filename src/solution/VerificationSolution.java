package solution;

import java.util.List;
import java.util.Map.Entry;

import model.Intervenant;


public class VerificationSolution {
	
	/**
	 * Méthode permettant de vérifier si une solution est correcte
	 * @param s Une solution avec des affectations
	 * @return True si la solution lue est correcte, false sinon
	 */
	@SuppressWarnings("unused")
	boolean verif(SolutionAvecAffectations s){
		for(Entry<Intervenant, List<AffectationIntervention>> entry : s.affectations.entrySet()){
			for (AffectationIntervention affint : entry.getValue()){
				// TODO
			}
		}
		return true;
	}
}
