/**
 * Classes permettant de modéliser un planning, appellé ici "solution" car c'est la 
 * terminologie habituelle dans le domaine de l'optimisation combinatoire pour
 * désigner le résultat de l'exécution d'un programme linéaire.
 * Contient un modèle de solution, un liseur de solution, un vérificateur de solution et 
 * une classe permettant de modéliser l'affectation d'un intervenant à une intervention.
 * <img src="./doc-files/dc_solution.png" alt="DC" />
 */
package solution;