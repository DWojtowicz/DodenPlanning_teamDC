/**
 * 
 */
package utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import main.Main;
import model.Adresse;
import model.Intervenant;
import model.Intervention;
import model.Usager;

/**
 * Singleton implémentant un parseur de CSV pour lire les fichiers CSV
 * que le programme prend en entrée.
 * @author charlotte
 *
 */
public class CSVReader {

	/**
	 * Le singleton
	 */
	private static volatile CSVReader INSTANCE = null;

	/**
	 * Chemin vers le fichier intervenants
	 */
	public Path pathFichierIntervenants;

	/**
	 * Chemin vers le fichier interventions
	 */
	public Path pathFichierIntervention;

	/**
	 * Chemin vers le fichier usagers
	 */
	public Path pathFichierUsagers;

	/**
	 * Constructeur privé, il faut utiliser getInstance pour récupérer
	 * une classe CSVReader
	 */
	private CSVReader() {}

	/**
	 * Fonction permettant de récupérer l'instance de CSVReader.
	 * @param pathFichierIntervenants Le chemin du fichier CSV contenant les intervenants
	 * @param pathFichierInterventions Le chemin du fichier CSV contenant les interventions
	 * @param pathFichierUsagers Le chemin du fichier CSV contenant les usagers
	 * @return L'instance de CSVReader
	 * @see https://en.wikipedia.org/wiki/Singleton_pattern
	 */
	public static CSVReader getInstance(Path pathFichierIntervenants, Path pathFichierInterventions, Path pathFichierUsagers) {
		if (INSTANCE == null) {
			synchronized(CSVReader.class) {
				if (INSTANCE == null) {
					INSTANCE = new CSVReader();
				}
				INSTANCE.pathFichierIntervenants = pathFichierIntervenants;
				INSTANCE.pathFichierIntervention = pathFichierInterventions;
				INSTANCE.pathFichierUsagers = pathFichierUsagers;
			}
		}
		return INSTANCE;
	}

	/**
	 * Fonction vérifiant si le CSV d'un intervenant est correct pour le format attendu pour le vrai utilisation
	 * ex: 0,31400,TOULOUSE, AVENUE DE  RANGUEIL,nom,prenom
	 * la case de la rue est tolérée vide
	 * @return true si le fichier CSV est correct, false sinon
	 */
	public boolean checkIntervenantsCSV() {
		Path file = pathFichierIntervenants;
		
		// Cas où aucun fichier n'est renseigné
		if (file == null)
			return false;
		
		List<String> lines;
		String[] toks;
		
		//initilisation d l'Id - they need to be in rising order
		@SuppressWarnings("unused")
		int first_id = Integer.MIN_VALUE;

		try {
			lines = Files.readAllLines(file);
			lines.remove(0);

			for (String line : lines){

				toks = line.split(",");

				//tester que le nombre de cases est correct
				if (toks.length != 6) {
					System.err.println("Nombre de cases incorrect - " + toks.length + " - " + line);
					return false;
				}
				
//				// le nombre de cases est ok
//
//					// ids need to be in rising order
//					if (Integer.parseInt(toks[0]) < Main.intervenants.get(Main.intervenants.size()-1).getIdIntervenant()) {
//						System.err.println("ID dans le désordre");
//						return false;
//					}

					//memorize the id of intervenant for next tour in loop - they need to be in rising order
					first_id = Integer.parseInt(toks[0]); 

					//verifier qu'une case n'est pas vide, sauf la case de la rue
					if (toks[0].length() == 0 || toks[1].length() == 0 || toks[2].length() == 0 || toks[3].length() == 0 || toks[4].length() == 0 || toks[5].length() == 0){
						System.err.println("Une ligne a une case vide");
						return false;
					}
				}

		} catch (IOException e) {
			System.err.println("problems during verification of intervenants");
			e.printStackTrace();
		}

		return true;
	}

	/**
	 * Fonction vérifiant si le CSV d'un usager est correct
	 * le format attentu est 6 colonnes: 
	 * id:int, code postale: int, ville: string, rue: string, nom: string, prénom: string
	 * @return true si le fichier CSV est correct, false sinon
	 */
	public boolean checkUsagerCSV() {
		Path file = pathFichierUsagers;
		
		// Cas où aucun fichier n'est renseigné
		if (file == null)
			return false;
		
		List<String> lines;
		String[] toks;

		try {
			lines = Files.readAllLines(file);
			lines.remove(0); // On ignore l'entête

			for (String line : lines){
				toks = line.split(",");

				//verifier qu'une case n'est pas vide
				if (toks.length == 6) {
					for (String tok : toks) {
						if (tok.length() == 0) {
							System.err.println("Une case est vide - ligne : " + line);
							return false;
						}
					}
				} else {
					System.err.println("Nombre de cases insuffisant - ligne : " + line);
					return false;
				}
					
			}

		} catch (IOException e) {
			System.err.println("problems during verification of usagers");
			e.printStackTrace();
		}

		return true;
	}

	/**
	 * Fonction vérifiant si le CSV des interventions est correct
	 * @return true si le fichier CSV est correct, false sinon
	 */
	public boolean checkInterventionsCSV() {
		Path file = pathFichierIntervention;

		// Cas où aucun fichier n'est renseigné
		if (file == null)
			return false;
		
		List<String> lines = null; 
		String[] toks;
		String[] heureDebut;
		String[] heureFin;
		int subDebutHeure;
		int subHeureFin;
		int subDebutMinute;
		int subFinMinute;

		try {
			lines = Files.readAllLines(file);
			lines.remove(0); // On ignore l'entête

			for (String line : lines) {

				toks = line.split(",");

				//si le nombre de colonnes est correcte
				if (toks.length == 5) {

					heureDebut = toks[3].split(":");
					heureFin = toks[4].split(":");

					//verifier qu'une case n'est pas vide
					for (String subToks : toks ){
						if (subToks.length() == 0) {
							System.err.println("Ligne avec case vide - ligne : " + line);
							return false;
						}
					}

					//verifier que l'heure de debut n'est pas plus grand que l'heure de fin
					subDebutHeure = Integer.parseInt(heureDebut[0]);
					subHeureFin = Integer.parseInt(heureFin[0]);
					subDebutMinute = Integer.parseInt(heureDebut[1]);
					subFinMinute = Integer.parseInt(heureFin[1]);

					if (subDebutHeure > subHeureFin) {
						System.err.println("Heures inconsistantes - ligne : " + line);
						return false;
					}

					//l'intervention commence le meme heure
					else if (subDebutHeure == subHeureFin && subDebutMinute > subFinMinute) {
						System.err.println("TimeWindow nulle - ligne : " + line);
						return false;
					}


				} else {
					// il n'y pas assez de colonnes dans une ligne
					System.err.println("La ligne n'a pas assez de cases");
					return false;
				}

			}

		} catch (IOException e) {
			System.err.println("problems during verification of interventions");
			e.printStackTrace();
		}

		return true;
	}

	/**
	 * Methode permettant d'importer une liste d'intervention supposé dans le meme jour
	 * On suppose le fichier d'interventions valide
	 * @param date des interventions sous forme de String "dd/mm/yyyy"
	 */
	@SuppressWarnings("deprecation")
	public void readInterventionsCSV(String date) {
		// Vérification de la date
		String[] toksDate;
		toksDate = date.split("/");
		if (toksDate.length != 3)
			throw new IllegalArgumentException("Format de date incorrect. Devrait être JJ/MM/AAA et était : " + date);



		// Initialisation des variables
		List<String> lines = null;
		String[] toks;
		String[] subToks;
		int duree;


		// Instanciation de la liste des interventions de main
		Main.INTERVENTIONS = new ArrayList<Intervention>();

		try {
			// Chargement des lignes du fichier CSV
			lines = Files.readAllLines(pathFichierIntervention);

			// Suppression de l'entête du CSV
			lines.remove(0);

			for (String line : lines) {

				// Initialisation des TW
				Date debutTW = new Date(Integer.parseInt(toksDate[2]), Integer.parseInt(toksDate[1]), Integer.parseInt(toksDate[0]));
				Date finTW = new Date(Integer.parseInt(toksDate[2]), Integer.parseInt(toksDate[1]), Integer.parseInt(toksDate[0]));

				// Décomposition de la ligne
				toks = line.split(",");

				// Durée en heures
				subToks = toks[1].split(":");
				duree = Integer.parseInt(subToks[0])*60;	// Heures
				duree += Integer.parseInt(subToks[1]);		// Minutes

				// Finalisation des time window
				subToks = toks[3].split(":");
//				debutTW.setHours(0);
				debutTW.setHours(Integer.parseInt(subToks[0]));
				debutTW.setMinutes(Integer.parseInt(subToks[1]));
				System.out.println(debutTW.toLocaleString());
				subToks = toks[4].split(":");
//				finTW.setHours(23);
				finTW.setHours(Integer.parseInt(subToks[0]));
				finTW.setMinutes(Integer.parseInt(subToks[1]));
				System.out.println(finTW.toLocaleString());

				// Récupération de l'usager
				Usager usager = Usager.getUsagerParID(Integer.parseInt(toks[0]));

				// Ajout de l'intervention
				Intervention i = new Intervention(
						Main.INTERVENTIONS.size(), usager, duree, debutTW, finTW);

				Main.INTERVENTIONS.add(i);
			}

		} catch (IOException e) {
			System.err.println("problems during import of interventions");
			e.printStackTrace();
		}
	}

	/**
	 * Méthode permettant d'importer une liste d'intervenants pour une "vraie" utilisation, cad contenant des noms et prenoms
	 * format attendu: id, code postal, ville, adresse, nom, prenom
	 */
	public void readIntervenantsCSV() {
		Path file = pathFichierIntervenants;

		List<String> lines;
		String[] toks;

		//instanciation de la liste des interventions de main
		Main.INTERVENANTS = new ArrayList<Intervenant>();

		try {

			lines = Files.readAllLines(file);
			lines.remove(0);

			for (String line : lines){

				toks = line.split(",");
				
				// Lecture de l'adresse
				
				// Construction de l'intervenant
				Main.INTERVENANTS.add(
						new Intervenant(
								Integer.parseInt(toks[0]),	// Le numéro de l'intervenant
								toks[5], 					// Le prénom de l'intervenant
								toks[4],					// Le nom de l'intervenant
								new Adresse(toks[3],toks[1],toks[2])));
			}

		} catch (IOException e) {
			System.err.println("problems during import of intervenants");
			e.printStackTrace();
		}

	}


	/**
	 * Méthode permettant d'importer une liste d'intervenants pour les tests: les noms sont generés aleatoirement
	 */
	public void readIntervenantsBenchCSV() {

		// Lecture du fichier
		readIntervenantsCSV();
		GiveName nomAleatoire;

		// Attribution de noms au hasard
		for (Intervenant i : Main.INTERVENANTS) {
			nomAleatoire = new GiveName();
			i.setPrenom(nomAleatoire.getPrenom());
			i.setNom(nomAleatoire.getNom());
		}

	}

	/**
	 *  methode permettant d'importer une liste d'usagers pour les tests: les noms sont generés aleatoirement
	 */
	public void readUsagerBenchCSV() {

		// Lecture du fichier
		readUsagerCSV();
		GiveName nomAleatoire;

		// Attribution de noms au hasard
		for (Usager u : Main.USAGERS) {
			nomAleatoire = new GiveName();
			u.setPrenom(nomAleatoire.getPrenom());
			u.setNom(nomAleatoire.getNom());
		}

	}

	/**
	 *  methode permettant d'importer une liste d'usagers 
	 * format attendu: id, code postal, ville, adresse, nom, prenom
	 */
	public void readUsagerCSV() {
		Path file = pathFichierUsagers;
		List<String> lines;
		String[] toks;

		//instanciation de la liste des interventions de main
		Main.USAGERS = new ArrayList<Usager>();

		try {

			lines = Files.readAllLines(file);
			lines.remove(0);

			for (String line : lines){
				toks = line.split(",");

				// Construction de l'usager
				Main.USAGERS.add(
						new Usager(
								Integer.parseInt(toks[0]),	// Le numéro de l'usager
								toks[5],					// Le prénom de l'usager
								toks[4],					// Le nom de l'usager
								new Adresse(toks[3],toks[1],toks[2])));

			}

		} catch (IOException e) {
			System.err.println("problems during import of clients");
			e.printStackTrace();
		}
	}
}
