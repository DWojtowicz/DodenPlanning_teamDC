package utils;

/**
 * Classe permettant d'obtenir des couples nom/prénom afin d'humaniser les plannings
 * @author charlotte
 *
 */
public class GiveName {
	
	/**
	 * La liste des prénoms les plus communs en France
	 */
	private static String[] listePrenoms = {"Marie","Nathalie","Isabelle","Sylvie","Catherine","Laurie","Christine","Françoise","Valérie","Sandrine","Stéphanie","Véronique","Sophie","Céline","Chantal","Patricia","Anne","Brigitte","Julie","Monique","Aurélie","Nicole","Laurence","Annie","Émilie","Dominique","Virginie","Corinne","Élodie","Christelle","Camille","Caroline","Léa","Sarah","Florence","Laetitia","Audrey","Hélène","Laura","Manon","Michèle","Cécile","Christiane","Béatrice","Claire","Nadine","Delphine","Pauline","Karine","Mélanie","Marion","Chloe","Jacqueline","Elisabeth","Evelyne","Marine","Claudine","Anais","Lucie","Danielle","Carole","Fabienne","Mathilde","Sandra","Pascale","Annick","Charlotte","Emma","Severine","Sabrina","Amandine","Myriam","Jocelyne","Alexandra","Angelique","Josiane","Joelle","Agnes","Mireille","Vanessa","Justine","Sonia","Bernadette","Emmanuelle","Oceane","Amelie","Clara","Maryse","Anne-marie","Fanny","Magali","Marie-christine","Morgane","Ines","Nadia","Muriel","Jessica","Laure","Genevieve","Estelle","Jean","Philippe","Michel","Alain","Patrick","Nicolas","Christophe","Pierre","Christian","Éric","Frédéric","Laurent","Stéphane","David","Pascal","Daniel","Alexandre","Julien","Thierry","Olivier","Bernard","Thomas","Sébastien","Gérard","Didier","Dominique","Vincent","François","Bruno","Guillaume","Jérôme","Jacques","Marc","Maxime","Romain","Claude","Antoine","Franck","Jean-Pierre","Anthony","Kévin","Gilles","Cédric","Serge","André","Mathieu","Benjamin","Patrice","Fabrice","Joël","Jérémy","Clément","Arnaud","Denis","Paul","Lucas","Hervé","Jean-Claude","Sylvain","Yves","Ludovic","Guy","Florian","Damien","Alexis","Mickaël","Quentin","Emmanuel","Louis","Benoît","Jean-Luc","Fabien","Francis","Hugo","Jonathan","Loïc","Xavier","Théo","Adrien","Raphaël","Jean-François","Grégory","Robert","Michaël","Valentin","Cyril","Jean-Marc","René","Lionel","Yannick","Enzo","Yannis","Jean-Michel","Baptiste","Matthieu","Rémi","Georges","Aurélien","Nathan","Jean-Paul"};

	/**
	 * Une liste de noms communs en France
	 */
	private static String[] listeNoms = {"MARTIN","DUPONT","MARIN","THOMAS","DURAND","DUPUIS","LAGAFFE","MEUNIER"};

	/**
	 * Le numéro du prénom tiré au sort
	 */
	private int randomPrenom;
	
	/**
	 * Le numéro du nom tiré au sort
	 */
	private int randomNom;

	/**
	 * Constructeur tirant au sort le numéro du prénom et du nom
	 */
	public GiveName() {
		changerPrenom();
		changerNom();
	}
	
	/**
	 * Fonction retournant un prénom dans notre liste
	 * @return Le prénom tité au sort
	 */
	public String getPrenom() {
		return listePrenoms[randomPrenom];
	}
	
	/**
	 * Fonction retournant un nom dans notre liste
	 * @return Le nom tiré au sort
	 */
	public String getNom() {
		return listeNoms[randomNom];
	}
	
	/**
	 * Procédure tirant au sort un prénom
	 */
	public void changerPrenom() {
		randomPrenom = (int) (Math.random() * listePrenoms.length);
	}
	
	/**
	 * Procédure tirant au sort un nom
	 */
	public void changerNom() {
		randomNom = (int) (Math.random() * listeNoms.length);
	}
	
}
