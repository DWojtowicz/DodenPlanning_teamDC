package utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import main.Main;
import model.Intervenant;
import model.Intervention;

/**
 * Singleton permettant de créer les fichiers nécessaires aux scripts Python et ZIMPL
 * @author damien
 *
 */
public class LinkPythonZIMPL {

	/**
	 * Le chemin vers le fichier listant les adresses pour le programme Python
	 */
	private static String PATH_FICHIER_ADRESSES = "python/adr.tmp";

	/**
	 * Le chemin vers le fichier des intervenants en entrée du programme ZIMPL
	 */
	private static String PATH_FICHIER_INTERVENANTS = "zimpl/intervenants.tmp";

	/**
	 * Le chemin vers le fichier des interventions en entrée du programme ZIMPL
	 */
	private static String PATH_FICHIER_INTERVENTIONS = "zimpl/interventions.tmp";

	/**
	 * La clé Google pour interroger Google Maps
	 */
	public static String CLE_GOOGLE = "";
	
	/**
	 * Le singleton
	 */
	private static volatile LinkPythonZIMPL INSTANCE = null;

	/**
	 * Constructeur privé, il faut utiliser getInstance pour récupérer
	 * une classe ReadSolution
	 */
	private LinkPythonZIMPL() {}

	/**
	 * Fonction permettant de récupérer l'instance de PythonZIMPLDataProcessor.
	 * @return L'instannce de ReadSolution
	 * @see https://en.wikipedia.org/wiki/Singleton_pattern
	 */
	public static LinkPythonZIMPL getInstance() {
		if (INSTANCE == null) {
			synchronized(LinkPythonZIMPL.class) {
				if (INSTANCE == null) {
					INSTANCE = new LinkPythonZIMPL();
				}
			}
		}
		return INSTANCE;
	}



	/**
	 * Fonction générant les fichiers d'interventions et d'intervenants pour le
	 * ZIMPL ainsi que le fichier des adresses pour le programme Python
	 * 
	 * TODO Éclater la fonction en trois sous-fonctions :
	 * 		TODO Le fichier d'adresses
	 * 		TODO Le fichier d'intervenants
	 * 		TODO Le fichier d'intervention
	 */
	public void generationFichiers(){
		List<String> coord = new ArrayList<>(); //coordonnées des interventions et les intervenants
		List<String> fixe = new ArrayList<>(); //les interventions
		List<String> mobile = new ArrayList<>(); //les intervenants

		Path file2 = Paths.get(LinkPythonZIMPL.PATH_FICHIER_ADRESSES);
		Path file3 = Paths.get(LinkPythonZIMPL.PATH_FICHIER_INTERVENANTS);
		Path file4 = Paths.get(LinkPythonZIMPL.PATH_FICHIER_INTERVENTIONS);

		/*
		 * Intervenants
		 */

		mobile.add("# number of intervenants");
		mobile.add(String.valueOf(Main.INTERVENANTS.size()));
		mobile.add("#NO  TdepartMIN TretourMax	TworkMax");

		fixe.add("#number of intervention\n" + Main.INTERVENTIONS.size() +"\n#NO TArrMin TdepartMax Tservice");
		//for(Intervenant i : intervenants){
		for(int temp = 0 ; temp < Main.INTERVENANTS.size() ; temp++){
			Intervenant i = Main.INTERVENANTS.get(temp);
			//String nom = String.valueOf(i.getIdIntervenant());
			String nom = String.valueOf(i.getIdIntervenant()+1);
			/*lines.add(nom + "\t"
					+i.getAdresse().getLatitude()+ "\t"
					+i.getAdresse().getLongitude());*/

			mobile.add(nom + "\t"
					+ "1.0\t23.0\t"
					+i.getNbHeures());
			if (i.getAdresse().getCommune().isEmpty()){
				coord.add(nom +"\t"+i.getAdresse().getCodePostal());
			}
			else{
				coord.add(nom +"\t"+i.getAdresse().getVoie()+ "," +
						i.getAdresse().getCodePostal() + " " +
						i.getAdresse().getCommune() );
			}

		}

		/*
		 * INTERVENTIONS
		 */
		
		Calendar cal = Calendar.getInstance();
		float debutTW;
		float finTW;
		
		for(int temp =0;temp<Main.INTERVENTIONS.size();temp++){
			//for( Intervention i : interventions){
			Intervention i = Main.INTERVENTIONS.get(temp);
			System.out.println(String.valueOf(i.getIdIntervention()));
			//String nom = String.valueOf(i.getIdIntervention());
			String nom =String.valueOf(i.getIdIntervention() +Main.INTERVENANTS.size()+1);
			/*lines.add(nom + "\t"
					+ i.getBeneficiaire().getAdresse().getLatitude() + "\t"
					+ i.getBeneficiaire().getAdresse().getLongitude()+ "\t"
					+ i.getPlagesDispo().get(0).getDebut()+ "\t"
					+ i.getPlagesDispo().get(0).getFin()+ "\t"
					+ i.getDuree()
					);*/

			cal.setTime(i.getDebutFenetreTemporelle());
			debutTW = (float)(cal.get(Calendar.HOUR_OF_DAY));
			debutTW += (float)(cal.get(Calendar.MINUTE)) / 60;
			
			cal.setTime(i.getFinFenetreTemporelle());
			finTW = (float)(cal.get(Calendar.HOUR_OF_DAY));
			finTW += (float)(cal.get(Calendar.MINUTE)) / 60;

			cal.setTime(i.getDebutFenetreTemporelle());
			
			fixe.add(nom + "\t"
					+ debutTW + "\t"
					+ finTW + "\t"
					+ (i.getDuree()/60.0));
			System.out.println(fixe.get(fixe.size()-1));
			if (i.getBeneficiaire().getAdresse().getCommune().isEmpty()){
				coord.add(nom +"\t"+i.getBeneficiaire().getAdresse().getCodePostal() );
			}
			else{
				coord.add(nom +"\t"+i.getBeneficiaire().getAdresse().getVoie()+ "," +
						i.getBeneficiaire().getAdresse().getCodePostal() + " " +
						i.getBeneficiaire().getAdresse().getCommune());
			}
		}
		try {
			Files.write(file2, coord, Charset.forName("UTF-8"));
			Files.write(file3, mobile, Charset.forName("UTF-8"));
			Files.write(file4, fixe, Charset.forName("UTF-8"));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Fonction générant les fichiers d'entrée du programme ZIMPL
	 */
	public void executionPython(){
		// Génération de la matrice des distances avec Google Maps
		try { // TODO Changer l'exec pour utiliser les variables static PATH...
			
			// On doit l'exécuter deux fois : une première fois pour enregistrer les données
			// une seconde fois pour les lire
			Process p = Runtime.getRuntime().exec("python2 python/matricedist.py " + CLE_GOOGLE);
			p.waitFor();
			p = Runtime.getRuntime().exec("python2 python/matricedist.py" + CLE_GOOGLE);
			p.waitFor();
		} catch (IOException e) {
			// TODO Implémenter le comportement. Voir avec les autres si besoin.
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Implémenter le comportement. Voir avec les autres si besoin.
			e.printStackTrace();
		}
	}


}
