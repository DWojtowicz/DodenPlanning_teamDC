package utils;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import main.Main;
/**
 * Classe permettant d'intéragir avec SCIP.
 * Il s'agit d'un singleton.
 * @author charlotte
 *
 */
public class LinkSCIP {

	/**
	 * Chemin du binaire de SCIP
	 */
	public static String PATH_TO_SCIP = "/opt/scipoptsuite-3.2.1/scip-3.2.1/bin/scip";

	/**
	 * Chemin du script d'exécution du programme SANS équilibrage de la charge de travail pour SCIP
	 */
	public static String PATH_TO_SCRIPT= "scip/lance_Scip.txt";

	/**
	 * Chemin du script d'exécution du programme AVEC équilibrage de la charge de travail pour SCIP
	 */
	public static String PATH_TO_SCRIPT_2= "scip/lance_Scip2.txt";
	
	/**
	 * Nombre de threads à utiliser
	 */
	public static String NB_THREADS = "2";

	/**
	 * Durée maximale acceptée pour la recherche d'une solution
	 */
	public static String MAX_EXEC_TIME = "300";

	/**
	 * Fichier de paramètres pour SCIP
	 */
	public static final String PARAMETER_FILE = "scip/scip.set";
	
	/**
	 * Le nom du fichier contenant la solution
	 */
	public static String NOM_FICHIER_RES_EXECUTION = "solutionSCIP.sol";

	/**
	 * Le chemin du fichier contenant la solution produite par SCIP
	 * On doit le différencier du nom du fichier, car le script de lancement de
	 * SCIP est dans le répertoire "scip", et écrit dans le fichier RES_EXEC
	 * relativement à sa position dans l'arborescence.
	 */
	public static String PATH_FICHIER_RES_EXECUTION = "scip/" + NOM_FICHIER_RES_EXECUTION;

	/**
	 * Le singleton
	 */
	private static volatile LinkSCIP INSTANCE = null;

	/**
	 * Constructeur privé, il faut utiliser getInstance pour récupérer
	 * une classe LinkSCIP
	 */
	private LinkSCIP() {}

	/**
	 * Fonction permettant de récupérer l'instance de LinkSCIP.
	 * @return L'instannce de LinkSCIP
	 * @see https://en.wikipedia.org/wiki/Singleton_pattern
	 */
	public static LinkSCIP getInstance() {
		if (INSTANCE == null) {
			synchronized(LinkSCIP.class) {
				if (INSTANCE == null) {
					INSTANCE = new LinkSCIP();
				}
			}
		}
		return INSTANCE;
	}


	/**
	 * Procédure d'exécution du programme ZIMPL dans SCIP
	 * Le problème à résoudre dépend de la valeur de Main.PROBLEME_A_RESOUDRE
	 */
	public void runSCIP() {

		// changer les parametres de SCIP
		changeParameterFile();

		/*
		 * Exécution de SCIP
		 */
		try {
			// run the SCIP script
			// using the Runtime exec method:
			Process p;
			
			// Choix du critère à optimiser
			switch (Main.PROBLEME_A_RESOUDRE) {
			case 0 :
				p = Runtime.getRuntime().exec( PATH_TO_SCIP + " -b " + PATH_TO_SCRIPT);
				System.out.println("OPTIMISATION");
				break;

			case 1 :
				p = Runtime.getRuntime().exec( PATH_TO_SCIP + " -b " + PATH_TO_SCRIPT_2);
				System.out.println("ÉQUILIBRAGE");
				break;
			
			default :
				throw new IllegalStateException("Le problème à résoudre est inconnu - n°" + Main.PROBLEME_A_RESOUDRE);
			}
			p.waitFor();
		
		} catch (IOException e) {
			// TODO Implémenter le comportement. Voir avec les autres si besoin.
			e.printStackTrace();
		
		} catch (InterruptedException e) {
			// TODO Implémenter le comportement. Voir avec les autres si besoin.
			e.printStackTrace();
		}
		
		
		
	}


	/**
	 * Fonction lancant l'exécution du programme ZIMPL avec SCIP
	 */
	public static void lanceSCIP() {

		try { 
			Process p = Runtime.getRuntime().exec( LinkSCIP.PATH_TO_SCIP + " -b " + LinkSCIP.PATH_TO_SCRIPT);
			p.waitFor();
		} catch (IOException e) {
			System.out.println("exception happened during execution of SCIP script, IO - here's what I know: ");
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("exception happened during execution of SCIP script, InterruptedException - here's what I know: ");
			e.printStackTrace();
		}
	}
	
	/***
	 * Fonction permettant de changer les parametres de SCIP a travers du fichier des parametres
	 * 
	 */
	public void changeParameterFile() {
		
		//open the parameter file
		File f = new File(PARAMETER_FILE);
		List<String> fileContent;
		try {
			
			
			//lire toute les lignes pour chercher les lignes contenant "limits/time = " et "lp/threads = " et remplacer le contenu
			fileContent = new ArrayList<>(Files.readAllLines(f.toPath(), StandardCharsets.UTF_8));

			for (int i = 0; i < fileContent.size(); i++) {
				if (fileContent.get(i).startsWith("limits/time"))
					fileContent.set(i, "limits/time = " + MAX_EXEC_TIME + "\n");
				else if (fileContent.get(i).startsWith("lp/threads"))
					fileContent.set(i, "lp/threads = " + NB_THREADS + "\n");
			}

			Files.write(f.toPath(), fileContent, StandardCharsets.UTF_8);


		} catch (IOException e) {
			System.out.println("exception happened during writing to parameter file - here's what I know: ");
			e.printStackTrace();
			System.exit(-1);        
		}
		
	}
	
	
}

