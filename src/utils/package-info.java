/**
 * Divers utilitaires permettant de faire l'interface avec le programme Python,
 * le solveur SCIP, d'instancier les classes modèles à partir du contenu des 
 * fichiers CSV, ainsi que d'"humaniser" des instances de tests.
 * <img src="./doc-files/dc_utils.png" alt="DC" />
 */
package utils;