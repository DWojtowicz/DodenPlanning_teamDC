#modifier le 30/03 20:06
param donnees_intervenants := "intervenants.tmp" ;
param donnees_interventions := "interventions.tmp" ;
param dist_time := "dist_time.tmp";
param nb_inte := read donnees_intervenants as "1n" comment "#" use 1 ;
param nb_serv := read donnees_interventions as "1n" comment "#" use 1 ;
do print nb_inte , " intervenant" ;
do print nb_serv , " intervention" ; 

set Indi := { read donnees_intervenants as "<1n>" comment "#" skip 1 use nb_inte} ;
set Indj := { read donnees_interventions as "<1n>" comment "#" skip 1 use nb_serv} ;
set Ind := Indi + Indj;
set mat := Ind * Ind ;
set paire := Indi * Indj ;
set paire2 := Indj * Indj ;
set paire3 := Indj * Indi ;
set paire4 := Indi * Indi ;

param TDepMin[Indi] := read donnees_intervenants as "<1n> 2n" comment "#" skip 1 use nb_inte ;
param TRetMax[Indi] := read donnees_intervenants as "<1n> 3n" comment "#" skip 1 use nb_inte ;
param TWorMax[Indi] := read donnees_intervenants as "<1n> 4n" comment "#" skip 1 use nb_inte ;

param TArriMin[Indj] :=  read donnees_interventions as "<1n> 2n" comment "#" skip 1 use nb_serv ;
param TLeavMax[Indj] :=  read donnees_interventions as "<1n> 3n" comment "#" skip 1 use nb_serv ;
param TServ[Indj] :=  read donnees_interventions as "<1n> 4n" comment "#" skip 1 use nb_serv ;

param dist[mat] := read dist_time as "<1n,2n> 3n" comment "#";
param TVoya[mat] := read dist_time as "<1n,2n> 4n" comment "#";

var x[mat] binary ;
var y[paire] binary ;
var idInte[Indj] integer >= 1 <= nb_inte ;
var TDepReal[Indi] real >=6 <= 20; 
var TRetReal[Indi] real >=6 <= 22;
var TWorReal[Indi] real >=0 <= 10;
var TArriReal[Indj] real >=7 <=20;
var TLeavReal[Indj] real >=7 <=22;

#objectif minimiser le temps gaspille total de intervenant 
minimize time_perdu: sum <i> in Indi :(TRetReal[i] - TDepReal[i] - TWorReal[i]) ;

# un predecesseur et un successeur
subto c1 : forall <j> in Ind do sum <i> in Ind : x[i,j] == 1 ;
subto c2 : forall <j> in Ind do sum <k> in Ind : x[j,k] == 1 ;

#time window pour intervenant;
subto c3 : forall <i> in Indi do TDepReal[i] >= TDepMin[i];
subto c4 : forall <i> in Indi do TRetReal[i] <= TRetMax[i];
subto c5 : forall <i> in Indi do TWorReal[i] <= TWorMax[i];
subto c6 : forall <i> in Indi do TRetReal[i] >= TDepReal[i] + TWorReal[i];

# pour chaque intervenant, le temps retoure effectue est >= le temps depart depuis une intervention plus le temps de rentrer
subto c7 : forall <i,j> in paire do 
	vif idInte[j] == i then TRetReal[i] >= TLeavReal[j] + TVoya[j,i] end;

#time window pour intervention;
# le temps d'arrive effectue chez client est >= temps d'arrive minimal du client
subto c8 : forall <j> in Indj do 
	 TArriReal[j] >= TArriMin[j];

# s'il existe une visit entre deux client,le temps d'arrive chez un client est >= le temps de depart de client predecesseur plus le temps de voyage entre le deux client
subto c9 : forall <i,j> in paire2 do # Indj * Indj
	vif x[i,j] == 1 then TArriReal[j] >= TLeavReal[i] + TVoya[i,j] end;

# s'il existe une visit entre intervenant et un intervention, le temps d'arrive effectue chez client est >= le temps depart effectue du intervenant plus le temps de voyage entre le deux
subto c10 : forall <i,j> in paire do # Indi * Indj
	vif x[i,j] == 1 then TArriReal[j] >= TDepReal[i] + TVoya[i,j] end;

# pour toutes les interventions, le temps de depart effectue depuis chez client est = le temps d'arrive effectue chez lui plus le temps de service
subto c11 : forall <j> in Indj do 
	TLeavReal[j] == TArriReal[j] + TServ[j] ; 

# pour toutes les interventions, le temps de depart effectue depuis chez client est <= le temps maximal de depart 
subto c12 : forall <j> in Indj do 
	TLeavReal[j] <= TLeavMax[j] ;

#equation de temps du travail effectue pour chaque intervenant
subto c13 : forall <i> in Indi do sum <j> in Indj : y[i,j] * TServ[j] == TWorReal[i];

# s'il existe visit entre deux interventions, then le intervenant est le meme personne
subto c14 : forall <i,j> in paire2 do # Indj * Indj
	vif x[i,j] == 1 then idInte[i] == idInte[j] end ;

# s'il existe visit entre intervenants et interventions, l'intervention est fait par cet intervenant 
subto c15 : forall <i,j> in paire do 
	vif x[i,j] == 1 then idInte[j] == i end ; 

# si le interventions sont fait par intervenant i alors varible y[i,j]=1, sinon y[i,j]=0
subto c16 : forall <i,j> in paire do 
	vif idInte[j] == i then y[i,j] == 1 else y[i,j] == 0 end ;






















	





